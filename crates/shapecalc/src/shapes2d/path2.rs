// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

use std::{cmp::Ordering, error::Error, iter, ops::Range};

use approx::{abs_diff_eq, abs_diff_ne};
use log::warn;
use nalgebra::{distance, Transform2, UnitVector2};
use thiserror::Error;

use crate::{
	traits::{
		multiply_round_decimals, BoundingRect, CoincidentIntersection2, DirectionAt, Flipable2, GenerateOffset2, HasBounds2, HasEnd2, HasLength, HasStart2, Intersection2, Intersection2Kind, IntersectsWith, Reverse, RoundedEq, RoundedOrd,
		SplitAt, TotalOrd, Transformable2,
	},
	EPSILON, FT, PI,
};

use super::{
	arc2::{arc2_get_type, arc2_new_from_type, Arc2Error, Arc2Type},
	line2::Line2Error,
	signed_angle_between, Arc2, Line2, Point2, Stroke2,
};

trait IntoStroke2 {
	type Error: Error;

	fn into_stroke(&self, start: Point2) -> Result<Stroke2, Self::Error>;
}

#[derive(Clone, Debug)]
struct Line2Segment {
	end: Point2,
}

impl Transformable2 for Line2Segment {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.end = transform * self.end;
		self
	}
}

impl Flipable2 for Line2Segment {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.end.flip(axis);
		self
	}
}

impl IntoStroke2 for Line2Segment {
	type Error = Line2Error;
	fn into_stroke(&self, start: Point2) -> Result<Stroke2, Self::Error> {
		Ok(Stroke2::Line2(Line2::new(start, self.end)?))
	}
}

impl From<&Line2> for Line2Segment {
	fn from(value: &Line2) -> Self {
		Line2Segment { end: value.end() }
	}
}

#[derive(Clone, Debug)]
struct Arc2Segment {
	arc_type: Arc2Type,
}

impl Transformable2 for Arc2Segment {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.arc_type.transform(transform);
		self
	}
}

impl Flipable2 for Arc2Segment {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.arc_type.flip(axis);
		self
	}
}

impl IntoStroke2 for Arc2Segment {
	type Error = Arc2Error;
	fn into_stroke(&self, start: Point2) -> Result<Stroke2, Self::Error> {
		Ok(arc2_new_from_type(start, self.arc_type.clone())?.into())
	}
}

impl From<&Arc2> for Arc2Segment {
	fn from(value: &Arc2) -> Self {
		Self { arc_type: arc2_get_type(value).clone() }
	}
}

#[derive(Error, Clone, Debug)]
enum Segment2Error {
	#[error(transparent)]
	Line2(#[from] Line2Error),
	#[error(transparent)]
	Arc2(#[from] Arc2Error),
}

#[derive(Clone, Debug)]
enum Segment2 {
	Line2(Line2Segment),
	Arc2(Arc2Segment),
}

impl From<Line2Segment> for Segment2 {
	fn from(value: Line2Segment) -> Self {
		Self::Line2(value)
	}
}

impl From<Arc2Segment> for Segment2 {
	fn from(value: Arc2Segment) -> Self {
		Self::Arc2(value)
	}
}

impl Transformable2 for Segment2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		match self {
			Segment2::Line2(l) => {
				l.transform(transform);
			}
			Segment2::Arc2(a) => {
				a.transform(transform);
			}
		}
		self
	}
}

impl Flipable2 for Segment2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		match self {
			Segment2::Line2(l) => {
				l.flip(axis);
			}
			Segment2::Arc2(a) => {
				a.flip(axis);
			}
		}
		self
	}
}

impl IntoStroke2 for Segment2 {
	type Error = Segment2Error;
	fn into_stroke(&self, start: Point2) -> Result<Stroke2, Self::Error> {
		Ok(match self {
			Segment2::Line2(l) => l.into_stroke(start)?,
			Segment2::Arc2(a) => a.into_stroke(start)?,
		})
	}
}

impl From<&Stroke2> for Segment2 {
	fn from(value: &Stroke2) -> Self {
		match value {
			Stroke2::Line2(l) => Segment2::Line2(l.into()),
			Stroke2::Arc2(a) => Segment2::Arc2(a.into()),
		}
	}
}

#[derive(Clone, Debug, Error)]
pub enum PathError {
	#[error("Path is self-intersecting")]
	SelfIntersecting,
	#[error("Path is not closed")]
	NotClosed,
}

#[derive(Clone, Debug, Error)]
pub enum PathPushError {
	#[error("The resulting stroke has zero length")]
	StrokeLengthZero,
	#[error("There was no start point found for the calculation")]
	NoMatchingStart,
}

/// A Path that may be closed
///
/// May also be just a single point
///
/// Guaranteed to have no consecutive segments that are coincident with each other.
// Todo?: lift consecutive coincident segments limitation?
#[derive(Clone, Debug)]
pub struct Path2 {
	/// Start point of the path (and end point if closed)
	start: Point2,
	/// Segments of the Path
	segments: Vec<Segment2>,
}

impl Path2 {
	pub fn new(start: Point2) -> Self {
		Self { start, segments: Vec::new() }
	}

	/// Adds the stroke to the end of the path.
	///
	/// The start or end point have to match the end of the path. (Error: NoMatchingStart)
	///
	/// If the end matches the stroke is reversed.
	///
	/// The stroke must not have a zero length. (Error: StrokeLengthZero)
	///
	/// *rounding_decimals* controls the rounding that is used when checking for point equality.
	pub fn push_stroke(&mut self, stroke: &Stroke2, rounding_decimals: Option<u32>) -> Result<(), PathPushError> {
		// Check length
		let length = stroke.length();
		if let Some(d) = rounding_decimals {
			if multiply_round_decimals(length, d) == 0 {
				return Err(PathPushError::StrokeLengthZero);
			}
		}

		let end = self.end();
		let matches_end = [stroke.start(), stroke.end()].map(|p| match rounding_decimals {
			Some(d) => p.rounded_eq(&end, d),
			None => abs_diff_eq!(p, end, epsilon = EPSILON),
		});

		let mut stroke = stroke.clone();
		if matches_end[0] {
		} else if matches_end[1] {
			stroke.reverse();
		} else {
			return Err(PathPushError::NoMatchingStart);
		}

		if let Err(e) = self.push_merge_segment((&stroke).into()) {
			warn!("Failed to add invalid segment to path: {}", e)
		}
		Ok(())
	}

	/// If `s` is coincident with the last existing segment: merges those segments if they point in the same direction.
	///
	/// If they do not point in the same direction: Ignores `s`.
	///
	/// Not coincident: Just pushes `s` onto `segments`
	///
	/// Invalid segment: returns PathPushError::StrokeLengthZero.
	fn push_merge_segment(&mut self, s: Segment2) -> Result<(), PathPushError> {
		let last_stroke = self.get_stroke(if self.segment_num() > 0 { self.segment_num() - 1 } else { 0 });
		let start = match &last_stroke {
			Some(s) => s.end(),
			None => self.start(),
		};
		let new_stroke = match s.into_stroke(start) {
			Ok(s) => s,
			Err(_) => return Err(PathPushError::StrokeLengthZero),
		};

		let (merged_segment, ignore_s): (Option<Segment2>, _) = match &last_stroke {
			Some(last) => match last {
				Stroke2::Line2(l0) => match new_stroke {
					Stroke2::Line2(l1) => {
						let direction0 = l0.direction();
						let direction1 = l1.direction();
						if abs_diff_eq!(direction0, direction1, epsilon = EPSILON) {
							// Same direction -> Coincident because they have a shared point
							(Some(Line2Segment { end: l1.end() }.into()), false)
						} else if abs_diff_eq!(direction0, -direction1, epsilon = EPSILON) {
							// Opposite direction -> ignore
							(None, true)
						} else {
							(None, false)
						}
					}
					Stroke2::Arc2(_) => (None, false),
				},
				Stroke2::Arc2(a0) => match new_stroke {
					Stroke2::Line2(_) => (None, false),
					Stroke2::Arc2(a1) => {
						if abs_diff_eq!(a0.center(), a1.center(), epsilon = EPSILON) && abs_diff_eq!(a0.radius(), a1.radius(), epsilon = EPSILON) {
							if a0.angle().signum() == a1.angle().signum() {
								(Some((&Stroke2::from(Arc2::new(a0.center(), a0.start(), a0.angle() + a1.angle()).expect("Expected arc to be valid"))).into()), false)
							} else {
								(None, true)
							}
						} else {
							(None, false)
						}
					}
				},
			},
			None => (None, false),
		};

		if let Some(s) = merged_segment {
			let idx = self.segment_num() - 1;
			self.segments[idx] = s;
		} else {
			if !ignore_s {
				self.segments.push(s);
			}
		}

		Ok(())
	}

	/// Appends the segments of p to self.
	pub fn append(&mut self, mut p: Path2) {
		if let Some(s) = p.segments.first() {
			if let Err(e) = self.push_merge_segment(s.clone()) {
				warn!("Failed to add invalid segment to path: {}", e)
			}
			self.segments.extend(p.segments.splice(1.., iter::empty()))
		}
	}

	/// Adjust path end point if the path would be closed with rounding.
	pub fn round_end(&mut self, rounding_decimals: Option<u32>) {
		let start = self.start();
		let end = self.end();
		if if let Some(d) = rounding_decimals { end.rounded_eq(&start, d) } else { abs_diff_eq!(start, end, epsilon = EPSILON) } {
			// The rounding function sets end = start. => exact comparison valid.
			// The rounding may have to be run multiple times, because if the last segment becomes invalid due to rounding, it is removed. Then the rounding has to be run again.
			while start != self.end() {
				self.set_segment_end(start, 1)
			}
		}
	}

	/// Adjust end of (segment with *self.segment_num() - neg_index*) to be the same as *new_end*
	fn set_segment_end(&mut self, new_end: Point2, neg_index: usize) {
		if self.segment_num() >= neg_index {
			{
				let index = self.segment_num() - neg_index;
				let segment = &mut self.segments[index];
				match segment {
					Segment2::Line2(l) => l.end = new_end,
					Segment2::Arc2(a) => match &mut a.arc_type {
						Arc2Type::Circle { .. } => {
							// Circle start and end point depend on segment before
							self.set_segment_end(new_end, neg_index + 1)
						}
						Arc2Type::Partial { end, .. } => {
							*end = new_end;
						}
					},
				}
			}
			{
				// Check if segment is still valid and remove if otherwise
				let index = self.segment_num() - neg_index;
				let stroke = self.try_get_stroke(index).expect("Expected index to be valid");
				if matches!(stroke, Err(_)) {
					self.segments.remove(index);
				}
			}
		}
	}

	pub fn push_line_to(&mut self, end: Point2) -> Result<(), PathPushError> {
		let current_end = self.end();
		let length = distance(&current_end, &end);

		if abs_diff_eq!(length, 0.0, epsilon = EPSILON) {
			return Err(PathPushError::StrokeLengthZero);
		}

		if let Err(e) = self.push_merge_segment(Line2Segment { end }.into()) {
			warn!("Failed to add invalid segment to path: {}", e)
		}

		Ok(())
	}

	pub fn from_strokes<'a, I: Iterator<Item = &'a Stroke2>>(strokes: I, rounding_decimals: Option<u32>, remove_duplicates: bool) -> Vec<Self> {
		// Bunch of helpers
		/// Shows if a stroke index refers to the start or end point of the stroke
		#[derive(Clone, Copy)]
		struct StartEnd {
			pub i: usize,
			pub is_start: bool,
		}
		fn get_startend_point(strokes: &[&Stroke2], index: StartEnd) -> Point2 {
			let stroke = strokes[index.i];
			if index.is_start {
				stroke.start()
			} else {
				stroke.end()
			}
		}
		/// Ordering based on comparing start/end points of strokes
		fn point_cmp_rounding(p1: Point2, p2: Point2, rounding_decimals: Option<u32>) -> Ordering {
			match rounding_decimals {
				Some(d) => p1.rounded_cmp(&p2, d),
				None => p1.total_cmp(&p2),
			}
		}
		/// Search for a range of indices from *s* where *f* returns *Ordering::Equal*
		fn binary_search_all_by<'a, T, F>(s: &'a [T], f: F) -> Range<usize>
		where
			F: Fn(&T) -> Ordering,
		{
			let low = s.partition_point(|a| {
				let ord = f(a);
				matches!(ord, Ordering::Less)
			});
			let high = s.partition_point(|a| {
				let ord = f(a);
				matches!(ord, Ordering::Less) || matches!(ord, Ordering::Equal)
			});
			return low..high;
		}

		// Actual fn start

		let strokes: Vec<_> = strokes
			.filter(|s| {
				// Removes any strokes with same start and end points. Strokes are guaranteed to have non-zero length, but with rounding that can change
				match rounding_decimals {
					Some(d) => {
						let valid = match s {
							Stroke2::Line2(l) => l.start().rounded_ne(&l.end(), d),
							Stroke2::Arc2(a) => a.center().rounded_ne(&a.start(), d),
						};
						if !valid {
							warn!("Stroke {:?} is invalid with rounding to {} decimals.", s, d);
							false
						} else {
							true
						}
					}
					None => true,
				}
			})
			.collect();
		// If the stroke at the codespendending index was already used for a path
		let mut was_used = vec![false; strokes.len()];

		let index_range = 0..strokes.len();
		// Stroke indices from *strokes* sorted by the start/end points
		let mut stroke_indices: Vec<_> = index_range.clone().map(|i| StartEnd { i, is_start: true }).chain(index_range.map(|i| StartEnd { i, is_start: false })).collect();
		stroke_indices.sort_by(|a, b| point_cmp_rounding(get_startend_point(&strokes, *a), get_startend_point(&strokes, *b), rounding_decimals));

		let mut paths = Vec::new();
		for stroke_idx in (&stroke_indices).iter() {
			if was_used[stroke_idx.i] {
				// Stroke has already been processed
				continue;
			}
			let stroke = strokes[stroke_idx.i];
			let mut path = Path2::new(stroke.start());
			let mut search_point = stroke.start();
			loop {
				let mut next_stroke = None;
				// Search for search_point by comparing with all start points
				let matching_indices = binary_search_all_by(&stroke_indices, |a| point_cmp_rounding(get_startend_point(&strokes, *a), search_point, rounding_decimals));
				for idx in matching_indices {
					let stroke_idx = stroke_indices[idx];
					if !was_used[stroke_idx.i] {
						let mut check_stroke = strokes[stroke_idx.i].clone();
						if !stroke_idx.is_start {
							check_stroke.reverse();
						}
						match &next_stroke {
							Some(next_stroke) => {
								if remove_duplicates {
									// Next stroke index for this iteration has already been found. We check for duplicate segments instead.
									if match rounding_decimals {
										Some(d) => check_stroke.rounded_eq(next_stroke, d),
										None => abs_diff_eq!(check_stroke, next_stroke, epsilon = EPSILON),
									} {
										was_used[stroke_idx.i] = true;
										warn!("Stroke {:?} already exists. Ignoring...", check_stroke)
									}
								}
							}
							None => {
								// Stroke was not yet processed; it becomes the next stroke
								was_used[stroke_idx.i] = true;

								if let Err(e) = path.push_merge_segment((&check_stroke).into()) {
									warn!("Failed to add invalid segment to path: {}", e)
								}
								next_stroke = Some(check_stroke)
							}
						}
					}
				}

				match &next_stroke {
					Some(next_stroke) => {
						search_point = next_stroke.end();
					}
					None => break,
				}
			}

			path.round_end(rounding_decimals);
			paths.push(path);
		}

		paths
	}

	/// If the Path is not just a single point and forms a closed loop
	pub fn is_closed(&self) -> bool {
		let end = self.end();
		self.segment_num() > 0 && abs_diff_eq!(self.start(), end, epsilon = EPSILON)
	}

	/// Returns intersections with itself
	pub fn self_intersections(&self) -> Vec<Intersection2> {
		let mut intersections: Vec<Intersection2> = Vec::new();

		let mut offset_iter = self.stroke_iter().enumerate();
		for stroke_info in self.stroke_iter() {
			let stroke = stroke_info.stroke;
			let mut stroke_intersections = Vec::new();

			offset_iter.next();
			let iter2 = offset_iter.clone();
			for (segment_idx, stroke_info2) in iter2 {
				let stroke_inters = stroke_info2.stroke.intersects_with(&stroke);
				let stroke_inters = stroke_inters.into_iter().filter_map(|mut intersection| {
					intersection.distance_a += stroke_info2.distance;
					intersection.distance_b += stroke_info.distance;
					if abs_diff_eq!(intersection.distance_a, intersection.distance_b, epsilon = EPSILON) {
						None
					} else {
						Some((intersection, segment_idx))
					}
				});
				stroke_intersections.extend(stroke_inters);
			}

			intersections.append(&mut self.handle_path_stroke_intersections(stroke_intersections));
		}

		intersections
	}

	/// Iterator over segments of self as strokes
	pub fn stroke_iter(&self) -> Stroke2Iterator<'_> {
		Stroke2Iterator::new(self)
	}

	/// Returns the stroke for the segment at *index*
	///
	/// If index is out of range: returns None
	///
	/// If the stroke is invalid: returns an error.
	///
	/// Segments/Strokes in a path are guaranteed to be valid for library users.
	/// This function is only meant for internal use to check if a segment/stroke is still valid or needs to be altered/removed.
	fn try_get_stroke(&self, index: usize) -> Option<Result<Stroke2, Segment2Error>> {
		if index >= self.segment_num() {
			return None;
		}

		let start = if index == 0 {
			self.start()
		} else {
			match &self.segments[index - 1] {
				Segment2::Line2(l) => l.end,
				Segment2::Arc2(a) => match a.arc_type {
					Arc2Type::Circle { .. } => {
						let stroke = self.try_get_stroke(index - 1).expect("Expected index to be valid");
						match stroke {
							Ok(s) => s.end(),
							Err(e) => return Some(Err(e)),
						}
					}
					Arc2Type::Partial { end, .. } => end,
				},
			}
		};

		let stroke = self.segments[index].into_stroke(start);
		Some(stroke)
	}

	/// Returns the stroke for the segment at *index*
	///
	/// If index is out of range: returns None
	pub fn get_stroke(&self, index: usize) -> Option<Stroke2> {
		let stroke = self.try_get_stroke(index);
		match stroke {
			Some(s) => Some(s.expect("Expected stroke to be valid")),
			None => None,
		}
	}

	pub fn segment_num(&self) -> usize {
		self.segments.len()
	}

	#[cfg(feature = "svg")]
	pub fn into_svg_data(&self) -> svg::node::element::path::Data {
		use svg::node::element::path::Data;

		use crate::traits::HasMiddle2;
		let start = self.start();
		let mut data = Data::new().move_to((start.x, start.y));
		for stroke in self.stroke_iter() {
			match stroke.stroke {
				Stroke2::Line2(l) => {
					let end = l.end();
					data = data.line_to((end.x, end.y));
				}
				Stroke2::Arc2(a) => {
					let end = a.end();
					let radius = a.radius();
					let angle = a.angle();
					if abs_diff_eq!(angle.abs(), 2.0 * PI, epsilon = EPSILON) {
						// Full circle
						let middle = a.middle();
						let sweep_flag = if angle >= 0.0 { 1 } else { 0 };
						data = data.elliptical_arc_to((radius, radius, 0, 0, sweep_flag, middle.x, middle.y));
						data = data.elliptical_arc_to((radius, radius, 0, 0, sweep_flag, end.x, end.y));
					} else {
						let (large_arc_flag, sweep_flag) = if angle > PI {
							(1, 1)
						} else if angle >= 0.0 {
							(0, 1)
						} else if angle < -PI {
							(1, 0)
						} else {
							(0, 0)
						};
						data = data.elliptical_arc_to((radius, radius, 0, large_arc_flag, sweep_flag, end.x, end.y));
					}
				}
			}
		}
		if self.is_closed() {
			data = data.close();
		}
		data
	}

	/// Moves the start point of the path to the specified distance.
	///
	/// If self is not closed: the part of the path before the new starting point will be discarded.
	///
	/// If `!self.is_closed() && distance > self.length()`: None will be returned.
	///
	/// If `self.is_closed() && distance > self.length()`: `distance % self.length` will be used as distance.
	pub fn open_at(self, mut distance: FT) -> Option<Path2> {
		let is_closed = self.is_closed();
		if distance >= self.length() && is_closed {
			distance = distance % self.length();
		}
		if distance <= 0.0 {
			return Some(self);
		}

		let (first_part, second_part) = self.split_at(distance);
		match second_part {
			Some(mut p) => {
				if is_closed {
					if let Some(f) = first_part {
						p.append(f);
					}
				}
				Some(p)
			}
			None => {
				if is_closed {
					first_part
				} else {
					None
				}
			}
		}
	}

	/// Calculates the ordering of the segments of the path
	///
	/// *Path must be closed and not self-intersecting*
	pub fn calculate_order(&self) -> Result<Path2Order, PathError> {
		if !self.is_closed() {
			return Err(PathError::NotClosed);
		}
		// Todo: check for self intersections

		let first_element = self.stroke_iter().next().expect("Expected at least one segment");

		let mut angle_sum = 0.0;
		if self.segment_num() < 3 {
			// Closed only possible with arcs => arcs take precedence
			for stroke in self.stroke_iter() {
				if let Stroke2::Arc2(a) = stroke.stroke {
					angle_sum += a.angle();
				}
			}
		} else {
			let mut iter = self.stroke_iter().peekable();
			while let Some(stroke) = &iter.next() {
				let stroke = &stroke.stroke;
				let dir0 = UnitVector2::new_normalize(stroke.end() - stroke.start());
				let next_stroke = match iter.peek() {
					Some(s) => &s.stroke,
					None => &first_element.stroke,
				};
				let dir1 = UnitVector2::new_normalize(next_stroke.end() - next_stroke.start());
				let signed_angle = signed_angle_between(dir0, dir1);
				angle_sum += signed_angle;
			}
		}

		Ok(if angle_sum >= 0.0 { Path2Order::CounterClockwise } else { Path2Order::Clockwise })
	}

	/// Handles intersections between a path and a stroke.
	///
	/// Deduplicates the intersections and calculates *crosses*
	///
	/// Each intersection of intersections has to be in format: (Intersection, segment index of the path the intersection belongs to).
	///
	/// The intersections have to be calculated in the order: (one of the strokes of the path).intersects_with(the singular stroke we are intersecting with)
	fn handle_path_stroke_intersections(&self, mut intersections: Vec<(Intersection2, usize)>) -> Vec<Intersection2> {
		/// Updates *crosses* for the intersections
		fn calculate_crossing(path: &Path2, inter: &mut Intersection2, segment_index: usize) {
			match &inter.kind {
				Intersection2Kind::Point(_) => {}
				Intersection2Kind::Coincident(c) => {
					if let CoincidentIntersection2::Overlap { o_a, o_b: _ } = c {
						if path.segment_num() <= 1 {
							return;
						}
						let before_idx = if segment_index == 0 {
							if path.is_closed() {
								path.segment_num() - 1
							} else {
								return;
							}
						} else {
							segment_index - 1
						};
						let after_idx = if segment_index == path.segment_num() - 1 {
							if path.is_closed() {
								0
							} else {
								return;
							}
						} else {
							segment_index + 1
						};
						let before = path.get_stroke(before_idx).expect("Expected index to be valid");
						let before_dir = before.direction_at(before.length());
						let o_start_dir = o_a.direction_at(0.0);
						let after = path.get_stroke(after_idx).expect("Expected index to be valid");
						let after_dir = after.direction_at(0.0);
						let o_end_dir = o_a.direction_at(o_a.length());

						fn is_to_the_right(base_dir: UnitVector2<FT>, dir: UnitVector2<FT>, stroke: &Stroke2) -> Option<bool> {
							let angle = signed_angle_between(base_dir, dir);
							if abs_diff_eq!(angle, 0.0, epsilon = EPSILON) {
								// directions are the same -> stroke and coincident intersection are coincident or stroke is an arc
								if let Stroke2::Arc2(a) = stroke {
									Some(!a.ccw())
								} else {
									None
								}
							} else {
								Some(angle < 0.0)
							}
						}

						// If the segment before points to the right
						let before_right = is_to_the_right(o_start_dir, before_dir, &before);
						// If the segment after points to the right
						let after_right = is_to_the_right(o_end_dir, after_dir, &after);

						if let Some(before_right) = before_right {
							if let Some(after_right) = after_right {
								inter.crosses = before_right != after_right;
							}
						}
					}
				}
			}
		}
		/// Calculates if current and last are duplicates or overlap each other and returns which one to remove
		///
		/// Result contains (add_current, remove_last)
		fn calculate_intersection_duplication(current: &Intersection2, last: &Intersection2) -> (bool, bool) {
			let (add_current, remove_last) = match &current.kind {
				Intersection2Kind::Point(p1) => (
					match &last.kind {
						Intersection2Kind::Point(p0) => {
							abs_diff_ne!(p0.p, p1.p, epsilon = EPSILON)
						}
						Intersection2Kind::Coincident(c) => match c {
							CoincidentIntersection2::Overlap { o_a, o_b: _ } => {
								abs_diff_ne!(o_a.end(), p1.p, epsilon = EPSILON)
							}
							CoincidentIntersection2::NoOverlap => true,
							CoincidentIntersection2::Point(p0) => {
								abs_diff_ne!(p0, &p1.p, epsilon = EPSILON)
							}
						},
					},
					false,
				),
				Intersection2Kind::Coincident(c1) => (
					true,
					match c1 {
						CoincidentIntersection2::Overlap { o_a, o_b: _ } => match &last.kind {
							Intersection2Kind::Point(p0) => abs_diff_eq!(p0.p, o_a.start(), epsilon = EPSILON),
							Intersection2Kind::Coincident(_) => {
								warn!("Consecutive coincident segments unsupported in Path2");
								false
							}
						},
						CoincidentIntersection2::NoOverlap => false,
						CoincidentIntersection2::Point(p1) => match &last.kind {
							Intersection2Kind::Point(p0) => abs_diff_eq!(p0.p, p1, epsilon = EPSILON),
							Intersection2Kind::Coincident(_) => {
								warn!("Consecutive coincident segments unsupported in Path2");
								false
							}
						},
					},
				),
			};
			(add_current, remove_last)
		}

		// Deduplicate intersections
		intersections.sort_by(|a, b| a.0.distance_a.total_cmp(&b.0.distance_a));
		let mut last_intersection: Option<Intersection2> = None;
		let mut deduplicated_intersections = Vec::new();
		while let Some((mut inter, segment_index)) = intersections.pop() {
			calculate_crossing(self, &mut inter, segment_index);

			if let Some(last_inter) = last_intersection {
				let (push_inter, pop_last) = calculate_intersection_duplication(&inter, &last_inter);
				if pop_last {
					deduplicated_intersections.pop();
				}
				if push_inter {
					deduplicated_intersections.push(inter.clone())
				}
			} else {
				deduplicated_intersections.push(inter.clone())
			}

			last_intersection = Some(inter);
		}

		// Check last and first intersection for closed paths
		if let Some(last_inter) = last_intersection {
			if deduplicated_intersections.len() > 1 && self.is_closed() {
				let (push_inter, pop_last) = calculate_intersection_duplication(&deduplicated_intersections[0], &last_inter);
				if pop_last {
					deduplicated_intersections.pop();
				}
				if !push_inter {
					deduplicated_intersections.remove(0);
				}
				// Todo: test case when first intersection is a coincident one. crosses?
			}
		}

		deduplicated_intersections
	}

	#[cfg(feature = "dxf")]
	pub fn from_dxf(drawing: &[dxf::entities::EntityType], rounding_decimals: Option<u32>) -> Vec<Self> {
		let mut strokes: Vec<Stroke2> = Vec::new();

		for e in drawing {
			match Stroke2::try_from(e) {
				Ok(s) => strokes.push(s),
				Err(e) => warn!("Error processing dxf entity: {}", e),
			}
		}

		Path2::from_strokes(strokes.iter(), rounding_decimals, true)
	}
}

pub enum Path2Order {
	Clockwise,
	CounterClockwise,
}

impl HasBounds2 for Path2 {
	fn bounding_rect(&self) -> crate::traits::BoundingRect {
		let start = self.start();

		let mut min_x = start.x;
		let mut max_x = start.x;
		let mut min_y = start.y;
		let mut max_y = start.y;

		for s in self.stroke_iter() {
			let rect = s.stroke.bounding_rect();
			min_x = min_x.min(rect.min.x);
			min_y = min_y.min(rect.min.y);
			max_x = max_x.max(rect.max.x);
			max_y = max_y.max(rect.max.y);
		}

		BoundingRect {
			min: Point2::new(min_x, min_y),
			max: Point2::new(max_x, max_y),
		}
	}
}

impl HasStart2 for Path2 {
	fn start(&self) -> Point2 {
		self.start
	}
}

impl HasEnd2 for Path2 {
	fn end(&self) -> Point2 {
		if self.segment_num() > 0 {
			let last_stroke = self.get_stroke(self.segment_num() - 1).expect("Expected index to be valid");
			last_stroke.end()
		} else {
			self.start()
		}
	}
}

impl HasLength for Path2 {
	fn length(&self) -> FT {
		let mut length = 0.0;
		if let Some(s) = self.stroke_iter().last() {
			length += s.distance + s.stroke.length();
		}
		length
	}
}

impl Transformable2 for Path2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.start = transform * self.start;
		for s in self.segments.iter_mut() {
			s.transform(transform);
		}
		self
	}
}

impl Flipable2 for Path2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.start.flip(axis);
		for s in self.segments.iter_mut() {
			s.flip(axis);
		}
		self
	}
}

impl Reverse for Path2 {
	fn reverse(&mut self) -> &mut Self {
		let mut reversed = Self::new(self.end());
		let mut reversed_segments: Vec<_> = self
			.stroke_iter()
			.map(|s| {
				let mut s = s.stroke;
				s.reverse();
				Segment2::from(&s)
			})
			.collect();
		reversed_segments.reverse();
		reversed.segments = reversed_segments;
		*self = reversed;
		self
	}
}

impl GenerateOffset2 for Path2 {
	fn generate_offset(&self, distance: FT) -> Option<Self> {
		if self.segment_num() == 0 {
			return None;
		}

		// Todo: check for self intersections

		/// Calculates the intersection between the last segment and the current segment + modifies the last segment accordingly.
		///
		/// Returns the intersection distance for the current segment.
		fn calculate_modify_intersection(current_segment: &Stroke2, segments: &mut Vec<Stroke2>) -> FT {
			let last_offset_segment = segments.last_mut();
			if let Some(last_offset_segment) = last_offset_segment {
				let mut intersections = last_offset_segment.intersects_with(current_segment);
				// Reversed sort; smallest distance last
				intersections.sort_by(|i0, i1| i1.distance_a.total_cmp(&i0.distance_a));
				let mut intersection = None;
				for i in intersections {
					match i.kind {
						Intersection2Kind::Point(_) => {
							intersection = Some(i);
						}
						Intersection2Kind::Coincident(_) => warn!("Consecutive coincident segments unsupported in Path2"),
					}
				}

				match intersection {
					Some(i) => {
						let (last_segment_adj, _) = last_offset_segment.clone().split_elongate_at(i.distance_a);
						match last_segment_adj {
							Some(l) => *last_offset_segment = l,
							None => {
								segments.pop();
							}
						}

						// Intersection distance on an arc is always positive. But intersection is "before" current segment -> negative distance if distance > arc.length
						match current_segment {
							Stroke2::Line2(_) => i.distance_b,
							Stroke2::Arc2(a) => {
								if i.distance_b > a.length() {
									-2.0 * PI * a.radius() + i.distance_b
								} else {
									i.distance_b
								}
							}
						}
					}
					None => 0.0,
				}
			} else {
				0.0
			}
		}

		let mut segments: Vec<Stroke2> = Vec::new();
		for stroke in self.stroke_iter() {
			let segment = stroke.stroke;
			let offset_segment = match segment.generate_offset(distance) {
				Some(s) => s,
				None => continue,
			};

			let intersection_distance = calculate_modify_intersection(&offset_segment, &mut segments);
			let (_, segment_adj) = offset_segment.split_elongate_at(intersection_distance);
			if let Some(s) = segment_adj {
				segments.push(s);
			}
		}

		if self.is_closed() {
			// Fix first and last segment
			if segments.len() >= 2 {
				let first_segment = segments.first().expect("Expected first segment to exist").clone();
				let intersection_distance = calculate_modify_intersection(&first_segment, &mut segments);
				let (_, segment_adj) = first_segment.split_elongate_at(intersection_distance);
				match segment_adj {
					Some(s) => segments[0] = s,
					None => {
						segments.remove(0);
					}
				}
			}
		}

		let mut offset_path = match segments.first() {
			Some(f) => Path2::new(f.start()),
			None => return None,
		};

		for s in segments {
			if let Err(e) = offset_path.push_merge_segment((&s).into()) {
				warn!("Failed to add invalid segment to path: {}", e)
			}
		}

		if self.is_closed() {
			offset_path.set_segment_end(offset_path.start(), 1);
		}

		// Check for self-intersections. Portions of the path between self intersections are removed depending on lower length.
		let mut self_intersections: Vec<_> = offset_path
			.self_intersections()
			.into_iter()
			.map(|i| {
				let small_dist = i.distance_a.min(i.distance_b);
				let big_dist = i.distance_a.max(i.distance_b);
				let i_length = big_dist - small_dist;
				let inv_i_length = offset_path.length() - big_dist + small_dist;
				// If the smaller length between the intersections goes over the start of the path
				let is_length_inv = inv_i_length < i_length && self.is_closed();
				(i, if is_length_inv { inv_i_length } else { i_length }, is_length_inv)
			})
			.collect();
		self_intersections.sort_by(|i0, i1| {
			// sort by shortest distance between distance_a and distance_b.
			i0.1.total_cmp(&i1.1)
		});
		while let Some((i, _, is_inv)) = self_intersections.pop() {
			let small_dist = i.distance_a.min(i.distance_b);
			let big_dist = i.distance_a.max(i.distance_b);

			let mut new_path = None;

			let big_split = offset_path.split_at(big_dist);
			if let Some(big_split_0) = big_split.0 {
				let small_split = big_split_0.split_at(small_dist);
				new_path = if is_inv {
					small_split.1
				} else {
					match small_split.0 {
						Some(mut ss) => match big_split.1 {
							Some(bs) => {
								ss.append(bs);
								Some(ss)
							}
							None => None,
						},
						None => None,
					}
				}
			}

			if let Some(p) = new_path {
				offset_path = p
			} else {
				return None;
			}

			// adjust intersection distances
			self_intersections = self_intersections
				.into_iter()
				.filter_map(|(mut i_adj, f1, f2)| {
					if is_inv {
						if i_adj.distance_a <= small_dist || i_adj.distance_b <= small_dist || i_adj.distance_a >= big_dist || i_adj.distance_b >= big_dist {
							None
						} else {
							i_adj.distance_a -= small_dist;
							i_adj.distance_b -= small_dist;
							Some((i_adj, f1, f2))
						}
					} else {
						if (i_adj.distance_a >= small_dist && i_adj.distance_a <= big_dist) || (i_adj.distance_b >= small_dist && i_adj.distance_b <= big_dist) {
							None
						} else {
							let section_length = big_dist - small_dist;
							if i_adj.distance_a >= big_dist {
								i_adj.distance_a -= section_length;
							}
							if i_adj.distance_b >= big_dist {
								i_adj.distance_b -= section_length;
							}
							Some((i_adj, f1, f2))
						}
					}
				})
				.collect();
		}

		Some(offset_path)
	}
}

impl SplitAt for Path2 {
	fn split_elongate_at(mut self, distance: FT) -> (Option<Self>, Option<Self>) {
		if self.segment_num() == 0 {
			return (None, None);
		}

		if distance <= 0.0 {
			let first_stroke = self.get_stroke(0).expect("Expected index to be valid");
			let (_, first_stroke) = first_stroke.split_elongate_at(distance);
			let path = match first_stroke {
				Some(f) => {
					self.start = f.start();
					self.segments[0] = (&f).into();
					Some(self)
				}
				None => None,
			};
			(None, path)
		} else if distance >= self.length() {
			let stroke_info = self.stroke_iter().last().expect("Expected at least one segment to exists");
			let (last_stroke, _) = stroke_info.stroke.split_elongate_at(distance - stroke_info.distance);
			let path = match last_stroke {
				Some(f) => {
					let last_idx = self.segment_num() - 1;
					self.segments[last_idx] = (&f).into();
					Some(self)
				}
				None => None,
			};
			(path, None)
		} else {
			let mut split_index = 0;
			let mut stroke_info = None;
			for (i, s) in self.stroke_iter().enumerate() {
				split_index = i;
				if s.distance + s.stroke.length() > distance {
					stroke_info = Some(s);
					break;
				}
				stroke_info = Some(s);
			}

			match stroke_info {
				Some(stroke_info) => {
					let (last_stroke, first_stroke) = stroke_info.stroke.split_elongate_at(distance - stroke_info.distance);
					let second_path = match first_stroke {
						Some(f) => {
							let mut path = Path2::new(f.start());
							path.segments.push((&f).into());
							path.segments.extend(self.segments.splice((split_index + 1).., iter::empty()));
							Some(path)
						}
						None => None,
					};
					let first_path = match last_stroke {
						Some(f) => {
							self.segments.truncate(split_index);
							self.segments.push((&f).into());
							Some(self)
						}
						None => None,
					};

					(first_path, second_path)
				}
				None => (None, None),
			}
		}
	}
}

/// Information about a stroke of a path
#[derive(Debug, Clone)]
pub struct Stroke2Info {
	/// Distance (on the Path) from the start of the Path to this stroke
	pub distance: FT,
	/// The stroke of this segment
	pub stroke: Stroke2,
}

/// Iterator over the individual strokes of a path
#[derive(Debug, Clone)]
pub struct Stroke2Iterator<'a> {
	path: &'a Path2,
	index: usize,
	current_distance: FT,
	current_start: Point2,
}

impl<'a> Stroke2Iterator<'a> {
	fn new(path: &'a Path2) -> Self {
		let start = path.start();
		Stroke2Iterator {
			path,
			index: 0,
			current_distance: 0.0,
			current_start: start,
		}
	}
}

impl<'a> Iterator for Stroke2Iterator<'a> {
	type Item = Stroke2Info;

	fn next(&mut self) -> Option<Self::Item> {
		if self.index < self.path.segments.len() {
			let stroke = self.path.segments[self.index].into_stroke(self.current_start).expect("Expected stroke to be valid");
			self.current_start = stroke.end();
			self.current_distance += stroke.length();
			self.index += 1;
			let info = Stroke2Info { distance: self.current_distance, stroke };
			Some(info)
		} else {
			None
		}
	}
}

impl IntersectsWith<Stroke2> for Path2 {
	fn intersects_with(&self, other: &Stroke2) -> Vec<Intersection2> {
		// TODO: boundingbox check

		let mut intersections: Vec<(Intersection2, usize)> = Vec::new();

		for (segment_idx, stroke_info) in self.stroke_iter().enumerate() {
			let stroke = stroke_info.stroke;
			let stroke_inters = stroke.intersects_with(other);
			let stroke_inters = stroke_inters.into_iter().map(|mut intersection| {
				intersection.distance_a = intersection.distance_a + stroke_info.distance;
				(intersection, segment_idx)
			});

			intersections.extend(stroke_inters);
		}

		self.handle_path_stroke_intersections(intersections)
	}
}

impl IntersectsWith<Path2> for Path2 {
	fn intersects_with(&self, other: &Path2) -> Vec<Intersection2> {
		let mut intersections = Vec::new();

		for s in other.stroke_iter() {
			let i = self.intersects_with(&s.stroke);
			intersections.extend(i.into_iter().map(|mut i| {
				i.distance_b += s.distance;
				i
			}))
		}

		intersections
	}
}
