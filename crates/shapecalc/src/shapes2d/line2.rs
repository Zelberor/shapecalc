// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

use std::cmp::Ordering;

use approx::{abs_diff_eq, abs_diff_ne, AbsDiffEq};
use nalgebra::{distance, Matrix2, Rotation2, Transform2, Translation2, UnitVector2};
use thiserror::Error;

use crate::{
	traits::{
		BoundingRect, CoincidentIntersection2, DirectionAt, DistanceTo, Flipable2, GenerateOffset2, HasBounds2, HasEnd2, HasLength, HasMiddle2, HasStart2, Intersection2, Intersection2Kind, IntersectsWith, PointIntersection2, Reverse,
		RoundedEq, RoundedOrd, SplitAt, TotalOrd, Transformable2,
	},
	EPSILON, FT, PI,
};

use super::{signed_angle_between, Arc2, Point2};

#[derive(Debug, Clone, PartialEq)]
pub struct Line2 {
	start: Point2,
	end: Point2,
}

#[cfg(feature = "dxf")]
impl TryFrom<&dxf::entities::Line> for Line2 {
	type Error = Line2Error;

	fn try_from(value: &dxf::entities::Line) -> Result<Self, Self::Error> {
		Line2::new(Point2::new(value.p1.x as FT, value.p1.y as FT), Point2::new(value.p2.x as FT, value.p2.y as FT))
	}
}

#[derive(Error, Clone, Debug)]
pub enum Line2Error {
	#[error("Line must not have zero length")]
	ZeroLength,
}

/// Line consisting of two points. Guaranteed to have non-zero length
impl Line2 {
	pub fn new(p1: Point2, p2: Point2) -> Result<Self, Line2Error> {
		if abs_diff_ne!(distance(&p1, &p2), 0.0, epsilon = EPSILON) {
			Ok(Line2 { start: p1, end: p2 })
		} else {
			Err(Line2Error::ZeroLength)
		}
	}

	/// If `distance > 0`: Move the point orthogonal "to the right" of self,
	/// when looking at self as a vertical line with p1 lower than p2.
	pub fn move_point_rangle(&self, p: Point2, distance: FT) -> Point2 {
		let line_dir = self.direction();
		let dir_90 = Rotation2::new(-PI / 2.0) * line_dir;
		Translation2::from(dir_90.into_inner() * distance) * p
	}

	/// Returns a parallel line with distance to self.
	///
	/// If `distance > 0`: Parallel line is "to the right" of self,
	/// when looking at self as a vertical line with p1 lower than p2.
	pub fn parallel(&self, distance: FT) -> Self {
		Line2 {
			start: self.move_point_rangle(self.start(), distance),
			end: self.move_point_rangle(self.end(), distance),
		}
	}

	/// Direction unit-vector of the line
	pub fn direction(&self) -> UnitVector2<FT> {
		let line_dir = self.end.coords - self.start.coords;
		UnitVector2::new_normalize(line_dir)
	}

	/// Calculates the values `(a, b, c)` to represent the line as the linear equation `ax + by = c`
	pub fn linear_equation(&self) -> (FT, FT, FT) {
		// Based on https://en.wikipedia.org/wiki/Line_(geometry)#Linear_equation
		let x0 = self.start().x;
		let x1 = self.end().x;
		let y0 = self.start().y;
		let y1 = self.end().y;

		let a;
		let b;
		let c;

		if abs_diff_ne!(x0, x1, epsilon = EPSILON) {
			a = (y1 - y0) / (x1 - x0);
			b = -1.0;
			c = -(x1 * y0 - x0 * y1) / (x1 - x0);
		} else {
			a = y1 - y0;
			b = 0.0;
			c = x0 * (y1 - y0);
		}

		(a, b, c)
	}

	/// Calculates the angle between two lines.
	/// - Angle > 0: counter clockwise
	/// - Angle < 0: clockwise
	pub fn signed_angle_to(&self, other: &Line2) -> FT {
		signed_angle_between(self.direction(), other.direction())
	}
}

impl DistanceTo<Point2, FT> for Line2 {
	fn distance_to(&self, other: &Point2) -> FT {
		let p0_p = match Line2::new(self.start, *other) {
			Ok(l) => l,
			Err(_) => return 0.0, // Distance of p1 and other is zero -> distance zero
		};

		let signed_angle = -self.signed_angle_to(&p0_p);
		let p0_p_distance = distance(&self.start, &other);

		p0_p_distance * signed_angle.sin()
	}
}

impl DistanceTo<Line2, FT> for Point2 {
	fn distance_to(&self, other: &Line2) -> FT {
		other.distance_to(self)
	}
}

impl RoundedEq for Line2 {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool {
		self.start().rounded_eq(&other.start(), decimals) && self.end().rounded_eq(&other.end, decimals)
	}
}

impl RoundedOrd for Line2 {
	fn rounded_cmp(&self, other: &Self, decimals: u32) -> Ordering {
		match self.start.rounded_cmp(&other.start, decimals) {
			Ordering::Equal => self.end.rounded_cmp(&other.end, decimals),
			ord => ord,
		}
	}
}

impl Transformable2 for Line2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.start = transform * self.start;
		self.end = transform * self.end;
		self
	}
}

impl AbsDiffEq for Line2 {
	type Epsilon = FT;

	fn default_epsilon() -> Self::Epsilon {
		FT::EPSILON
	}

	fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
		self.start().abs_diff_eq(&other.start, epsilon) && self.end().abs_diff_eq(&other.end, epsilon)
	}
}

impl IntersectsWith<Line2> for Line2 {
	fn intersects_with(&self, other: &Line2) -> Vec<Intersection2> {
		// Based on https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line_segment
		let x1 = self.start().x;
		let x2 = self.end().x;
		let y1 = self.start().y;
		let y2 = self.end().y;

		let x3 = other.start().x;
		let x4 = other.end().x;
		let y3 = other.start().y;
		let y4 = other.end().y;

		let divisor = Matrix2::new(x1 - x2, x3 - x4, y1 - y2, y3 - y4).determinant();

		if abs_diff_eq!(divisor, 0.0, epsilon = EPSILON) {
			if abs_diff_eq!(self.distance_to(&other.start), 0.0, epsilon = EPSILON) {
				let mut points = [self.start, self.end, other.start, other.end];
				points.sort_by(|a, b| a.total_cmp(b));
				let overlaps = distance(&points[0], &points[3]) <= self.length() + other.length();
				// Points of the overlapping section / section in-between
				let c_p0 = points[1];
				let c_p1 = points[2];

				/// Calculates the distance from the line to the nearest point on the line
				///
				/// Return (distance, nearest_p, farthest_p)
				fn calculate_distance(l: &Line2, p0: Point2, p1: Point2) -> (FT, Point2, Point2) {
					let start = l.start();
					let d_p0 = distance(&start, &p0);
					let d_p1 = distance(&start, &p1);

					if d_p0 < d_p1 {
						(d_p0, p0, p1)
					} else {
						(d_p1, p1, p0)
					}
				}

				let (distance_a, distance_b, coincident) = if overlaps {
					// Calculating sign unnecessary here because distance is always positive for overlapping intersections
					let (distance_a, p0_a, p1_a) = calculate_distance(self, c_p0, c_p1);
					let (distance_b, p0_b, p1_b) = calculate_distance(other, c_p0, c_p1);
					(
						distance_a,
						distance_b,
						match Line2::new(p0_a, p1_a) {
							Ok(l) => CoincidentIntersection2::Overlap {
								o_a: l.into(),
								o_b: Line2::new(p0_b, p1_b).expect("Line with same points is already valid").into(),
							},
							Err(_) => CoincidentIntersection2::Point(p0_a),
						},
					)
				} else {
					let (distance_a, p0_a, _) = calculate_distance(self, other.start(), other.end());
					let (distance_b, p0_b, _) = calculate_distance(other, self.start(), self.end());
					(distance_a * calculate_distance_sign(self, p0_a), distance_b * calculate_distance_sign(other, p0_b), CoincidentIntersection2::NoOverlap)
				};

				vec![Intersection2 {
					distance_a,
					distance_b,
					in_bounds: overlaps,
					crosses: false,
					kind: Intersection2Kind::Coincident(coincident),
				}]
			} else {
				// No intersections, lines are parallel
				Vec::new()
			}
		} else {
			let t_dividend = Matrix2::new(x1 - x3, x3 - x4, y1 - y3, y3 - y4).determinant();
			let u_dividend = Matrix2::new(x1 - x3, x1 - x2, y1 - y3, y1 - y2).determinant();
			let dividends = [t_dividend, u_dividend];
			let mut in_bounds = true;

			for d in dividends {
				if !(abs_diff_eq!(d, 0.0, epsilon = EPSILON) || (d.signum() == divisor.signum() && d.abs() <= divisor.abs())) {
					in_bounds = false;
				}
			}
			let t = t_dividend / divisor;

			let p_x = x1 + t * (x2 - x1);
			let p_y = y1 + t * (y2 - y1);
			let p = Point2::new(p_x, p_y);

			vec![Intersection2 {
				// todo: add tests for distances
				distance_a: distance(&self.start(), &p) * calculate_distance_sign(self, p),
				distance_b: distance(&other.start(), &p) * calculate_distance_sign(other, p),
				in_bounds,
				crosses: true,
				kind: Intersection2Kind::Point(PointIntersection2 {
					p,
					direction_a: self.direction(),
					direction_b: other.direction(),
				}),
			}]
		}
	}
}

/// Library internal use only.
///
/// Calculates if a point on the line is in bounds.
pub fn point_in_line_bounds(line: &Line2, p: Point2) -> bool {
	let dist_start = distance(&line.start(), &p);
	let dist_end = distance(&line.end(), &p);
	let line_length = line.length();

	dist_start <= line_length && dist_end <= line_length
}

/// Library internal use only.
///
/// Calculates the sign of a intersection distance from l.start() to p.
pub fn calculate_distance_sign(l: &Line2, p: Point2) -> FT {
	let dir = p - l.start();
	if l.direction().angle(&dir) < PI * 0.5 {
		1.0
	} else {
		-1.0
	}
}

impl HasBounds2 for Line2 {
	fn bounding_rect(&self) -> crate::traits::BoundingRect {
		let p0 = self.start();
		let p1 = self.end();
		let (min_x, max_x) = if p0.x > p1.x { (p1.x, p0.x) } else { (p0.x, p1.x) };
		let (min_y, max_y) = if p0.y > p1.y { (p1.y, p0.y) } else { (p0.y, p1.y) };

		BoundingRect {
			min: Point2::new(min_x, min_y),
			max: Point2::new(max_x, max_y),
		}
	}
}

impl HasStart2 for Line2 {
	#[inline]
	fn start(&self) -> Point2 {
		self.start
	}
}

impl HasEnd2 for Line2 {
	#[inline]
	fn end(&self) -> Point2 {
		self.end
	}
}

impl HasMiddle2 for Line2 {
	fn middle(&self) -> Point2 {
		Translation2::from(self.direction().into_inner() * self.length() * 0.5) * self.start()
	}
}

impl HasLength for Line2 {
	fn length(&self) -> FT {
		distance(&self.start, &self.end)
	}
}

impl Reverse for Line2 {
	fn reverse(&mut self) -> &mut Self {
		let end = self.start();
		self.start = self.end();
		self.end = end;
		self
	}
}

impl Flipable2 for Line2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.start.flip(axis);
		self.end.flip(axis);
		self
	}
}

impl GenerateOffset2 for Line2 {
	fn generate_offset(&self, distance: FT) -> Option<Self> {
		Some(self.parallel(distance))
	}
}

impl SplitAt for Line2 {
	fn split_elongate_at(self, distance: FT) -> (Option<Self>, Option<Self>) {
		let dir = self.direction();
		let split_point = Translation2::from(dir.into_inner() * distance) * self.start();
		let first_line = Line2::new(self.start(), split_point).map_or(None, |l| Some(l));
		let second_line = Line2::new(split_point, self.end()).map_or(None, |l| Some(l));
		if distance <= 0.0 {
			(None, second_line)
		} else if distance >= self.length() {
			(first_line, None)
		} else {
			(first_line, second_line)
		}
	}
}

impl DirectionAt for Line2 {
	fn direction_at(&self, _: FT) -> UnitVector2<FT> {
		self.direction()
	}
}

impl IntersectsWith<Arc2> for Line2 {
	fn intersects_with(&self, other: &Arc2) -> Vec<Intersection2> {
		other
			.intersects_with(self)
			.into_iter()
			.map(|mut i| {
				i.flip_a_b();
				i
			})
			.collect()
	}
}

#[cfg(test)]
mod tests {
	use approx::assert_abs_diff_eq;
	use nalgebra::distance;

	use crate::{
		shapes2d::tests::assert_compare_intersections,
		traits::{CoincidentIntersection2, DistanceTo, HasBounds2, HasEnd2, HasLength, HasStart2, Intersection2, Intersection2Kind, IntersectsWith, PointIntersection2, Reverse},
		EPSILON, PI,
	};

	use super::{Line2, Line2Error, Point2};

	#[test]
	fn point_distance_positive() {
		let line = Line2::new(Point2::new(0.0, 0.0), Point2::new(0.0, 2.0)).unwrap();
		let point = Point2::new(1.0, 1.0);
		let distance = line.distance_to(&point);
		assert_abs_diff_eq!(distance, 1.0, epsilon = EPSILON);
	}

	#[test]
	fn point_distance_negative() {
		let line = Line2::new(Point2::new(0.0, 0.0), Point2::new(0.0, 2.0)).unwrap();
		let point = Point2::new(-1.0, 1.0);
		let distance = line.distance_to(&point);
		assert_abs_diff_eq!(distance, -1.0, epsilon = EPSILON);
	}

	#[test]
	fn line_signed_angle_to_90deg_positive() {
		let line0 = Line2::new(Point2::new(2.0, 4.0), Point2::new(8.0, 4.0)).unwrap();
		let line1 = Line2::new(Point2::new(1.0, 2.0), Point2::new(1.0, 3.0)).unwrap();
		let angle = line0.signed_angle_to(&line1);
		assert_abs_diff_eq!(angle, PI * 0.5, epsilon = EPSILON);
	}

	#[test]
	fn signed_angle_to_90deg_negative() {
		let line1 = Line2::new(Point2::new(2.0, 4.0), Point2::new(8.0, 4.0)).unwrap();
		let line0 = Line2::new(Point2::new(1.0, 2.0), Point2::new(1.0, 3.0)).unwrap();
		let angle = line0.signed_angle_to(&line1);
		assert_abs_diff_eq!(angle, -PI * 0.5, epsilon = EPSILON);
	}

	#[test]
	fn err_for_zero_length() {
		let result = Line2::new(Point2::new(2.0, 4.0), Point2::new(2.0, 4.0));

		assert!(matches!(result, Err(Line2Error::ZeroLength)));
	}

	#[test]
	fn bounding_rect() {
		let line0 = Line2::new(Point2::new(2.0, 4.0), Point2::new(8.0, 3.0)).unwrap();
		let bounding_rect = line0.bounding_rect();
		assert_abs_diff_eq!(bounding_rect.min, Point2::new(2.0, 3.0), epsilon = EPSILON);
		assert_abs_diff_eq!(bounding_rect.max, Point2::new(8.0, 4.0), epsilon = EPSILON);
	}

	#[test]
	fn reverse() {
		let line0 = Line2::new(Point2::new(2.0, 4.0), Point2::new(8.0, 3.0)).unwrap();
		let mut line1 = line0.clone();
		line1.reverse();
		assert_abs_diff_eq!(line0.start(), line1.end(), epsilon = EPSILON);
		assert_abs_diff_eq!(line0.end(), line1.start(), epsilon = EPSILON);
	}

	#[test]
	fn linear_equation() {
		todo!()
	}

	#[test]
	fn intersection_line_point_in_bounds() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(2.5, 0.0), Point2::new(2.5, 4.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: line0.length() * 0.5,
			distance_b: 2.5,
			in_bounds: true,
			crosses: true,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(2.5, 2.5),
				direction_a: line0.direction(),
				direction_b: line1.direction(),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_point_in_bounds_edge() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(2.5, 0.0), Point2::new(2.5, 2.5)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: line0.length() * 0.5,
			distance_b: 2.5,
			in_bounds: true,
			crosses: true,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(2.5, 2.5),
				direction_a: line0.direction(),
				direction_b: line1.direction(),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_point_out_of_bounds() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(2.5, 0.0), Point2::new(2.5, 2.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: line0.length() * 0.5,
			distance_b: 2.5,
			in_bounds: false,
			crosses: true,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(2.5, 2.5),
				direction_a: line0.direction(),
				direction_b: line1.direction(),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_none_parallel() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(0.0, -1.0), Point2::new(5.0, 4.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 0);
	}

	#[test]
	fn intersection_line_coincident_in_bounds_stroke() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(2.5, 2.5), Point2::new(10.0, 10.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: line0.length() * 0.5,
			distance_b: 0.0,
			in_bounds: true,
			crosses: false,
			kind: Intersection2Kind::Coincident(CoincidentIntersection2::Overlap {
				o_a: Line2::new(Point2::new(2.5, 2.5), Point2::new(5.0, 5.0)).unwrap().into(),
				o_b: Line2::new(Point2::new(2.5, 2.5), Point2::new(5.0, 5.0)).unwrap().into(),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_coincident_in_bounds_point() {
		let line0 = Line2::new(Point2::new(0.0, 0.0), Point2::new(5.0, 5.0)).unwrap();
		let line1 = Line2::new(Point2::new(5.0, 5.0), Point2::new(10.0, 10.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: line0.length(),
			distance_b: 0.0,
			in_bounds: true,
			crosses: false,
			kind: Intersection2Kind::Coincident(CoincidentIntersection2::Point(Point2::new(5.0, 5.0))),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_coincident_out_of_bounds() {
		let l0_start = Point2::new(0.0, 0.0);
		let line0 = Line2::new(l0_start, Point2::new(-5.0, 2.5)).unwrap();
		let l1_start = Point2::new(1.0, -0.5);
		let line1 = Line2::new(l1_start, Point2::new(100.0, -50.0)).unwrap();

		let intersections = line0.intersects_with(&line1);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let start_distance = distance(&l0_start, &l1_start);
		let c = Intersection2 {
			distance_a: -start_distance,
			distance_b: -start_distance,
			in_bounds: false,
			crosses: false,
			kind: Intersection2Kind::Coincident(CoincidentIntersection2::NoOverlap),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_arc_point_dual_in_bounds_check_for_correct_flip() {
		todo!()
	}
}
