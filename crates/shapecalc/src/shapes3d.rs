// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

use crate::FT;

pub type Point3 = nalgebra::Point3<FT>;
