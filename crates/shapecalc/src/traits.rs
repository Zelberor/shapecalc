// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

use std::cmp::Ordering;

use approx::{abs_diff_eq, abs_diff_ne};
use nalgebra::{Transform2, UnitVector2};

use crate::{
	shapes2d::{Point2, Stroke2},
	EPSILON, FT,
};

#[derive(Clone, Debug)]
/// An intersection in 2d space
pub struct Intersection2 {
	/// Distance to the start of self.
	/// - > 0: Distance in same direction of self
	/// - < 0: Distance in opposite direction of self
	///
	/// If the shape is cyclic (e.g. Arc2) the distance is always positive (No way to determine if a point is before or after the start).
	pub distance_a: FT,
	/// Distance to the start of other.
	/// - > 0: Distance in same direction of other
	/// - < 0: Distance in opposite direction of other
	///
	/// If the shape is cyclic (e.g. Arc2) the distance is always positive (No way to determine if a point is before or after the start).
	pub distance_b: FT,
	/// If the two shapes actually intersect. (Important for lines where the intersection point is reported even if it is not inside the line bounds)
	pub in_bounds: bool,
	/// If other crosses self. (Example: It does not cross, if the shapes just touch or are coincident)
	pub crosses: bool,
	/// The intersection kind
	pub kind: Intersection2Kind,
}

impl Intersection2 {
	/// Flips the fields of intersection participants a and b
	pub fn flip_a_b(&mut self) -> &mut Self {
		match &mut self.kind {
			Intersection2Kind::Point(p) => {
				let direction_b = p.direction_a;
				p.direction_b = p.direction_a;
				p.direction_b = direction_b;
			}
			Intersection2Kind::Coincident(c) => match c {
				CoincidentIntersection2::Overlap { o_a, o_b } => {
					let overlap_b = o_a.clone();
					*o_a = o_b.clone();
					*o_b = overlap_b;
				}
				_ => {}
			},
		};
		let distance_b = self.distance_a;
		self.distance_a = self.distance_b;
		self.distance_b = distance_b;

		self
	}
}

#[derive(Clone, Debug)]
/// A coincident intersection
pub enum CoincidentIntersection2 {
	/// The coincident shapes overlap: Overlapping section.
	///
	/// The overlapping stroke is oriented so that its direction at the start matches the direction of (a=self/b=other) at the intersection point.
	Overlap { o_a: Stroke2, o_b: Stroke2 },
	/// The coincident shapes do not overlap: Section between
	NoOverlap,
	/// The coincident shapes touch in exactly one point
	Point(Point2),
}

#[derive(Clone, Debug)]
/// A point intersection
pub struct PointIntersection2 {
	/// Intersection point
	pub p: Point2,
	/// Direction of self at the intersection point
	pub direction_a: UnitVector2<FT>,
	/// Direction of other at the intersection point
	pub direction_b: UnitVector2<FT>,
}

#[derive(Clone, Debug)]
/// The kind of intersection
pub enum Intersection2Kind {
	/// One intersection point.
	Point(PointIntersection2),
	/// Coincident section
	Coincident(CoincidentIntersection2),
}

pub trait IntersectsWith<T> {
	/// Returns the intersections between two shapes
	///
	/// If returning a list, all elements are expected to be unique.
	/// Coincident sections also must be unique. (No other intersection points on the coincident section)
	fn intersects_with(&self, other: &T) -> Vec<Intersection2>;
}

pub trait DistanceTo<T, FT> {
	fn distance_to(&self, other: &T) -> FT;
}

pub trait DirectionAt {
	/// Calculates the tangential direction of the stroke at the specified distance from the start point
	/// - `distance > 0`: in direction of the arc
	/// - `distance < 0`: opposite direction of the arc
	fn direction_at(&self, distance: FT) -> UnitVector2<FT>;
}

/// Total ordering with rounded values.
///
/// Values are rounded to a certain number of decimals
///
/// The *multiply_round_decimals* function should be used for rounding.
pub trait RoundedEq {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool;
	fn rounded_ne(&self, other: &Self, decimals: u32) -> bool {
		!self.rounded_eq(other, decimals)
	}
}

impl RoundedEq for FT {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool {
		multiply_round_decimals(*self, decimals) == multiply_round_decimals(*other, decimals)
	}
}

impl RoundedOrd for FT {
	fn rounded_cmp(&self, other: &Self, decimals: u32) -> Ordering {
		multiply_round_decimals(*self, decimals).cmp(&multiply_round_decimals(*other, decimals))
	}
}

/// Total ordering with rounded values.
///
/// Values are rounded to a certain number of decimals
///
/// The *multiply_round_decimals* function should be used for rounding.
///
/// For multidimensional data, first the x-Axis should take precedence, then the y-Axis, z-Axis, etc.
pub trait RoundedOrd: RoundedEq {
	/// Total ordering with rounded values.
	fn rounded_cmp(&self, other: &Self, decimals: u32) -> Ordering;
}

/// Multiplies by a certain number of decimals and then rounds.
pub fn multiply_round_decimals(x: FT, decimals: u32) -> i64 {
	let y = 10i64.pow(decimals) as f64;
	(x * y).round() as i64
}

/// Total ordering for DataTypes that usually do not have it (containing floats)
///
/// For multidimensional data, first the x-Axis should take precedence, then the y-Axis, z-Axis, etc.
pub trait TotalOrd {
	/// Total ordering for DataTypes that usually do not have it (containing floats)
	fn total_cmp(&self, other: &Self) -> Ordering;
}

/// Total ordering based on distance(self, reference) vs. distance(other, reference)
pub trait RefDistanceOrd {
	/// Total ordering based on distance(self, reference) vs. distance(other, reference)
	fn ref_distance_cmp(&self, other: &Self, reference: &Self) -> Ordering;
}

pub trait IntoApproximate<T> {
	/// Approximates the given shape with another shape(s)
	fn into_approximate(&self, linear_deviation: FT, angular_deviation: FT) -> T;
}

#[derive(Debug, Clone, Default)]
pub struct BoundingRect {
	pub min: Point2,
	pub max: Point2,
}

impl BoundingRect {
	pub fn width(&self) -> FT {
		(self.max.x - self.min.x).abs()
	}

	pub fn height(&self) -> FT {
		(self.max.y - self.min.y).abs()
	}

	/// The BoundingRect is invalid, if it has a size of 0.0 in any direction
	pub fn is_valid(&self) -> bool {
		abs_diff_ne!(self.width(), 0.0) && abs_diff_ne!(self.height(), 0.0)
	}

	pub fn combined_with(&self, other: &Self) -> Self {
		if !self.is_valid() && other.is_valid() {
			return other.clone();
		} else if self.is_valid() && !other.is_valid() {
			return self.clone();
		}

		let min_x = self.min.x.min(other.min.x);
		let max_x = self.max.x.max(other.max.x);
		let min_y = self.min.y.min(other.min.y);
		let max_y = self.max.y.max(other.max.y);

		BoundingRect {
			min: Point2::new(min_x, min_y),
			max: Point2::new(max_x, max_y),
		}
	}
}

pub trait HasBounds2 {
	fn bounding_rect(&self) -> BoundingRect;
}

impl<B: HasBounds2> HasBounds2 for Vec<B> {
	/// Note that this implementation returns a BoundingRect::default when the Vec is empty
	fn bounding_rect(&self) -> BoundingRect {
		let mut iter = self.iter();
		match iter.next() {
			Some(first) => {
				let mut result = first.bounding_rect();
				for i in iter {
					result = result.combined_with(&i.bounding_rect());
				}
				result
			}
			None => BoundingRect::default(),
		}
	}
}

impl<B: HasBounds2> HasBounds2 for Option<B> {
	fn bounding_rect(&self) -> BoundingRect {
		match self {
			Some(b) => b.bounding_rect(),
			None => BoundingRect::default(),
		}
	}
}

pub trait HasStart2 {
	fn start(&self) -> Point2;
}

pub trait HasEnd2 {
	fn end(&self) -> Point2;
}

pub trait HasMiddle2 {
	/// Middle point of the stroke
	fn middle(&self) -> Point2;
}

pub trait HasLength {
	/// Length of the shape
	fn length(&self) -> FT;
}

pub trait Transformable2 {
	/// Transforms self with the *transform* matrix
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self;
}

impl<T: Transformable2> Transformable2 for Vec<T> {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.iter_mut().for_each(|i| {
			i.transform(transform);
		});
		self
	}
}

pub trait Flipable2 {
	/// Flips/Mirrors self across *axis*
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self;
}

impl<F: Flipable2> Flipable2 for Vec<F> {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.iter_mut().for_each(|i| {
			i.flip(axis);
		});
		self
	}
}

pub trait GenerateOffset2: Sized {
	/// Generates a clone of self that has been offset by *distance*.
	///
	/// If `distance > 0`: New clone is "to the right" of self.
	///
	/// None is returned if the resulting shape is invalid
	fn generate_offset(&self, distance: FT) -> Option<Self>;
}

pub trait SplitAt: Sized + HasLength {
	/// Splits self at distance from start or elongates self if distance is out of bounds.
	///
	/// - if `distance <= 0`: tuple contains (None, self with distance.abs() added to the start position)
	/// - if `distance >= self.length`: tuple contains (self with distance added to the end position, None)
	/// - else: tuple contains (part before split, part after split)
	fn split_elongate_at(self, distance: FT) -> (Option<Self>, Option<Self>);

	/// Splits self at distance from start.
	///
	/// - if `distance <= 0`: tuple contains (None, unmodified self)
	/// - if `distance >= self.length`: tuple contains (unmodified self, None)
	/// - else: tuple contains (part before split, part after split)
	fn split_at(self, distance: FT) -> (Option<Self>, Option<Self>) {
		if distance <= 0.0 {
			(None, Some(self))
		} else if distance >= self.length() {
			(Some(self), None)
		} else {
			self.split_elongate_at(distance)
		}
	}

	/// Shortens self for the specified *amount*
	///
	/// If the shortened amount is bigger than the length of self the result is None
	///
	/// - `amount > 0`: The shape is shortened at the end
	/// - `amount < 0`: The shape is shortened at the start
	fn shorten(self, amount: FT) -> Option<Self> {
		if abs_diff_eq!(amount, 0.0, epsilon = EPSILON) {
			Some(self)
		} else if amount > 0.0 {
			let distance = self.length() - amount;
			let (result, _) = self.split_at(distance);
			result
		} else {
			// amount < 0.0
			let distance = amount;
			let (_, result) = self.split_at(distance);
			result
		}
	}

	/// Elongates self for the specified *amount*
	///
	/// If the shape has gotten invalid due to elongation the result is None
	///
	/// - `amount > 0`: The shape is elongated at the end
	/// - `amount < 0`: The shape is elongated at the start
	fn elongate(self, amount: FT) -> Option<Self> {
		if abs_diff_eq!(amount, 0.0, epsilon = EPSILON) {
			Some(self)
		} else if amount > 0.0 {
			let distance = self.length() + amount;
			let (result, _) = self.split_at(distance);
			result
		} else {
			// amount < 0.0
			let distance = -amount;
			let (_, result) = self.split_at(distance);
			result
		}
	}
}

pub trait Reverse {
	fn reverse(&mut self) -> &mut Self;
}
