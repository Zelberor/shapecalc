// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

pub mod shapes2d;
pub mod shapes3d;
pub mod traits;

// The library expects all angles to be in radians

pub type FT = f64;
pub use std::f64::consts::PI;
pub const EPSILON: FT = FT::EPSILON * 8.0;
