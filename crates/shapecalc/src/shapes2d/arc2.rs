// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

use approx::{abs_diff_eq, assert_abs_diff_eq, AbsDiffEq};
use log::warn;
use nalgebra::{distance, Rotation2, Transform2, Translation2, UnitVector2, Vector2};
use thiserror::Error;

use crate::{
	traits::{
		BoundingRect, DirectionAt, Flipable2, GenerateOffset2, HasBounds2, HasEnd2, HasLength, HasMiddle2, HasStart2, Intersection2, Intersection2Kind, IntersectsWith, IntoApproximate, PointIntersection2, Reverse, RoundedEq, SplitAt,
		Transformable2,
	},
	EPSILON, FT, PI,
};

use super::{
	line2::{calculate_distance_sign, point_in_line_bounds, Line2},
	Point2,
};

/// Due to start and end point being the same when the arc is a full circle, circles and partial arcs have to be handled separately.
///
/// This design was chosen to make sure the start and end points are always well-defined (not calculated on the fly) to minimize rounding-related bugs.
#[derive(Clone, Debug, PartialEq)]
pub enum Arc2Type {
	/// The arc is a full circle.
	///
	/// - True => counter-clockwise rotation
	/// - False => clockwise rotation
	Circle { ccw: bool, center: Point2 },
	/// The arc is not a full circle.
	///
	/// Angle of the arc in radians.
	/// Angle is guaranteed to be in interval \]-2pi, +2pi\[.
	///
	/// - Positive => counter-clockwise rotation
	/// - Negative => clockwise rotation
	Partial { angle: FT, end: Point2 },
}

impl Arc2Type {
	#[allow(dead_code)]
	pub fn ccw(&self) -> bool {
		match self {
			Arc2Type::Circle { ccw, .. } => *ccw,
			Arc2Type::Partial { angle, .. } => *angle > 0.0,
		}
	}

	pub fn angle(&self) -> FT {
		match self {
			Arc2Type::Circle { ccw, .. } => 2.0 * PI * if *ccw { 1.0 } else { -1.0 },
			Arc2Type::Partial { angle, .. } => *angle,
		}
	}
}

impl Transformable2 for Arc2Type {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		match self {
			Arc2Type::Circle { ccw: _, center } => {
				*center = transform * *center;
			}
			Arc2Type::Partial { angle: _, end } => {
				*end = transform * *end;
			}
		}

		self
	}
}

impl Flipable2 for Arc2Type {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		match self {
			Arc2Type::Circle { ccw, center } => {
				*ccw = !*ccw;
				center.flip(axis);
			}
			Arc2Type::Partial { angle, end } => {
				*angle = -*angle;
				end.flip(axis);
			}
		}
		self
	}
}

/// Returns the type of the arc
///
/// Only meant for usage in Path2, not meant for public api.
pub fn arc2_get_type(a: &Arc2) -> &Arc2Type {
	&a.arc_type
}

/// Creates an arc from an arc-type
///
/// Only meant for usage in Path2, not meant for public api.
pub fn arc2_new_from_type(start: Point2, t: Arc2Type) -> Result<Arc2, Arc2Error> {
	match t {
		Arc2Type::Circle { ccw: _, center } => {
			if abs_diff_eq!(distance(&start, &center), 0.0, epsilon = EPSILON) {
				return Err(Arc2Error::ZeroRadius);
			}
		}
		Arc2Type::Partial { angle, end } => {
			if abs_diff_eq!(distance(&start, &end), 0.0, epsilon = EPSILON) || abs_diff_eq!(angle, 0.0, epsilon = EPSILON) {
				return Err(Arc2Error::ZeroAngle);
			}
		}
	}

	Ok(Arc2 { start, arc_type: t })
}

/// Arc. Guaranteed to have non-zero angle and non-zero radius.
/// Can also be a full circle
#[derive(Clone, Debug, PartialEq)]
pub struct Arc2 {
	/// Start point of the arc
	start: Point2,
	/// Type of the arc.
	///
	/// Handles difference between full and partial circles.
	arc_type: Arc2Type,
}

#[derive(Error, Clone, Debug)]
pub enum Arc2Error {
	#[error("Arc angle must not be zero")]
	ZeroAngle,
	#[error("Arc radius must not be zero")]
	ZeroRadius,
}

impl Arc2 {
	/// Creates a new arc.
	///
	/// Angle should be in interval \[-2pi, +2pi].
	///
	/// If the angle lies not in this interval, a warning is emitted and angle is clamped.
	pub fn new(center: Point2, start: Point2, angle: FT) -> Result<Self, Arc2Error> {
		if abs_diff_eq!(angle, 0.0, epsilon = EPSILON) {
			return Err(Arc2Error::ZeroAngle);
		}
		let arc = Self {
			start,
			arc_type: if abs_diff_eq!(angle.abs(), 2.0 * PI, epsilon = EPSILON) || angle.abs() > 2.0 * PI {
				if angle.abs() > 2.0 * PI {
					warn!("Arc angle.abs() {} is bigger than 2.0*PI. Value will be clamped", angle);
				}
				Arc2Type::Circle { ccw: angle > 0.0, center }
			} else {
				Arc2Type::Partial {
					angle,
					end: Translation2::from(center) * Rotation2::new(angle) * Translation2::from(-center) * start,
				}
			},
		};

		if abs_diff_eq!(arc.radius(), 0.0, epsilon = EPSILON) {
			Err(Arc2Error::ZeroRadius)
		} else {
			Ok(arc)
		}
	}

	/// Creates a new arc.
	///
	/// The arc is based on the positions of start + center and the angle between start and end.
	///
	/// If end is not on the arc, then the arc will be created anyway based on this angle.
	pub fn new2(center: Point2, start: Point2, end: Point2, ccw: bool) -> Result<Self, Arc2Error> {
		let base = match Line2::new(center, start) {
			Ok(l) => l,
			Err(_) => return Err(Arc2Error::ZeroRadius),
		};
		let center_end = match Line2::new(center, end) {
			Ok(l) => l,
			Err(_) => return Err(Arc2Error::ZeroRadius),
		};

		let signed_angle = base.signed_angle_to(&center_end);
		let arc = Self {
			start,
			arc_type: if abs_diff_eq!(signed_angle, 0.0, epsilon = EPSILON) || abs_diff_eq!(start, end, epsilon = EPSILON) {
				Arc2Type::Circle { ccw, center }
			} else {
				let angle = if (signed_angle > 0.0 && ccw) || (signed_angle < 0.0 && !ccw) {
					signed_angle
				} else {
					signed_angle - 2.0 * PI * signed_angle.signum()
				};

				Arc2Type::Partial {
					angle,
					end: Translation2::from(center) * Rotation2::new(angle) * Translation2::from(-center) * start,
				}
			},
		};

		Ok(arc)
	}

	pub fn center(&self) -> Point2 {
		match self.arc_type {
			Arc2Type::Circle { ccw: _, center } => center,
			Arc2Type::Partial { angle, end } => {
				let se_distance = distance(&self.start(), &end);
				let center_distance = se_distance * 0.5 / (angle * 0.5).tan();

				let se_dir = UnitVector2::new_normalize(end - self.start());
				let center_offset = center_distance * (Rotation2::new(0.5 * PI) * se_dir).into_inner();

				let center = Translation2::from(se_dir.into_inner() * se_distance * 0.5 + center_offset) * self.start();
				center
			}
		}
	}

	pub fn angle(&self) -> FT {
		self.arc_type.angle()
	}

	pub fn radius(&self) -> FT {
		distance(&self.center(), &self.start)
	}

	/// Returns if the arc is counter-clockwise
	pub fn ccw(&self) -> bool {
		self.arc_type.ccw()
	}

	/// Calculates the point on the arc at the specified angle from the start point
	/// - angle positive => counter-clockwise rotation
	/// - angle negative => clockwise rotation
	///
	/// *Ignores the arc direction.*
	pub fn point_at_angle(&self, angle: FT) -> Point2 {
		let center = self.center();
		Translation2::from(center) * Rotation2::new(angle) * Translation2::from(-center) * self.start()
	}

	/// Calculates the point on the arc at the specified distance from the start point
	/// - `distance > 0`: in direction of the arc
	/// - `distance < 0`: opposite direction of the arc
	pub fn point_at(&self, distance: FT) -> Point2 {
		let circumference = 2.0 * PI * self.radius();
		let angle = ((distance % circumference) / circumference) * 2.0 * PI * self.angle().signum();
		self.point_at_angle(angle)
	}

	/// Calculates the intersection points between self and a linear equation.
	///
	/// The linear equation `(a, b, c)` has the form `ax + by = c`
	///
	/// **The arc center has to be (0, 0) otherwise this function panics.**
	///
	/// The functions returns a list of (Intersection point, distance on the arc of the intersection to arc.start(), if the intersection is in_bounds of the arc angle, if the intersection crosses)
	fn intersection_linear_equation(&self, linear_equation: (FT, FT, FT)) -> Vec<(Point2, FT, bool, bool)> {
		// Based on https://en.wikipedia.org/wiki/Intersection_(geometry)#A_line_and_a_circle
		// Arc center has to be (0,0) for the calculation
		let center = self.center();
		assert_abs_diff_eq!(center.x, 0.0, epsilon = EPSILON);
		assert_abs_diff_eq!(center.y, 0.0, epsilon = EPSILON);

		let (a, b, c) = linear_equation;
		let r = self.radius();

		let root = r * r * (a * a + b * b) - c * c;
		if abs_diff_eq!(root, 0.0, epsilon = EPSILON) {
			// One intersection point
			let div = a * a + b * b;
			let x = (a * c) / div;
			let y = (b * c) / div;
			let p = Point2::new(x, y);

			let (distance, in_bounds) = self.arc_intersection_bounds_check(p);
			vec![(p, distance, in_bounds, false)]
		} else if root < 0.0 {
			// No intersection
			Vec::new()
		} else {
			// Two intersections
			let sqrt = root.sqrt();
			let div = a * a + b * b;
			let x0 = (a * c + b * sqrt) / div;
			let y0 = (b * c - a * sqrt) / div;
			let x1 = (a * c - b * sqrt) / div;
			let y1 = (b * c + a * sqrt) / div;

			let p0 = Point2::new(x0, y0);
			let p1 = Point2::new(x1, y1);

			let (distance0, in_bounds0) = self.arc_intersection_bounds_check(p0);
			let (distance1, in_bounds1) = self.arc_intersection_bounds_check(p1);
			vec![(p0, distance0, in_bounds0, true), (p1, distance1, in_bounds1, true)]
		}
	}

	/// Calculates (distance, in_bounds) for an intersection point on an arc.
	///
	/// distance is always positive
	///
	/// **p has to be on the circle of the arc** for the result to be valid
	fn arc_intersection_bounds_check(&self, p: Point2) -> (FT, bool) {
		let base = Line2::new(self.center(), self.start()).expect("Expected radius to be non-zero");
		let center_p = Line2::new(self.center(), p).expect("Expected point to be on arc circle");
		let arc_angle = self.angle();
		let arc_radius = self.radius();

		let signed_p_angle = base.signed_angle_to(&center_p);
		if abs_diff_eq!(signed_p_angle, 0.0, epsilon = EPSILON) {
			return (0.0, true);
		}
		if arc_angle > 0.0 {
			if signed_p_angle > 0.0 {
				let distance = signed_p_angle * arc_radius;
				(distance, signed_p_angle <= arc_angle)
			} else {
				let angle = 2.0 * PI + signed_p_angle;
				let distance = angle * arc_radius;
				(distance, angle <= arc_angle)
			}
		} else {
			if signed_p_angle > 0.0 {
				let angle = -2.0 * PI + signed_p_angle;
				let distance = angle.abs() * arc_radius;
				(distance, angle >= arc_angle)
			} else {
				let distance = signed_p_angle.abs() * arc_radius;
				(distance, signed_p_angle >= arc_angle)
			}
		}
	}
}

#[cfg(feature = "dxf")]
impl TryFrom<&dxf::entities::Circle> for Arc2 {
	type Error = Arc2Error;

	fn try_from(value: &dxf::entities::Circle) -> Result<Self, Self::Error> {
		let center = Point2::new(value.center.x, value.center.y);
		Arc2::new(center, Translation2::new(value.radius, 0.0) * center, 2.0 * PI)
	}
}

#[cfg(feature = "dxf")]
impl TryFrom<&dxf::entities::Arc> for Arc2 {
	type Error = Arc2Error;

	fn try_from(value: &dxf::entities::Arc) -> Result<Self, Self::Error> {
		let start_angle = value.start_angle / 180.0 * PI;
		let end_angle = value.end_angle / 180.0 * PI;

		let angle = end_angle - start_angle;
		let center = Point2::new(value.center.x as FT, value.center.y as FT);

		let start_point = Translation2::from(center) * Rotation2::new(start_angle) * Point2::new(value.radius, 0.0);
		Arc2::new(center, start_point, angle)
	}
}

impl IntoApproximate<Vec<Line2>> for Arc2 {
	fn into_approximate(&self, linear_deviation: FT, angular_deviation: FT) -> Vec<Line2> {
		let mut lines = Vec::new();

		let angle = self.angle();
		let center = self.center();
		let mut segment_num = (self.length() / linear_deviation).ceil() as u64;
		let mut segment_angle = angle / (segment_num as FT);
		if segment_angle > angular_deviation {
			segment_num = (angle / angular_deviation).ceil() as u64;
			segment_angle = angle / (segment_num as FT);
		}
		let transform = Translation2::from(center) * Rotation2::new(segment_angle) * Translation2::from(-center);
		let mut start_point = self.start();
		for _ in 0..segment_num {
			let end_point = transform * start_point;
			lines.push(Line2::new(start_point, end_point).expect("Expected lines to have non-zero length"));
			start_point = end_point;
		}
		lines
	}
}

impl Transformable2 for Arc2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		self.start = transform * self.start;
		self.arc_type.transform(transform);
		self
	}
}

impl HasBounds2 for Arc2 {
	fn bounding_rect(&self) -> crate::traits::BoundingRect {
		let radius = self.radius();
		let center = self.center();
		let extreme_points = [
			Point2::new(center.x - radius, center.y),
			Point2::new(center.x + radius, center.y),
			Point2::new(center.x, center.y - radius),
			Point2::new(center.x, center.y + radius),
		];
		let iter = extreme_points
			.into_iter()
			.filter(|p| {
				let (_, in_bounds) = self.arc_intersection_bounds_check(*p);
				in_bounds
			})
			.chain([self.start(), self.end()]);

		let mut max_x = FT::NEG_INFINITY;
		let mut min_x = FT::INFINITY;
		let mut max_y = FT::NEG_INFINITY;
		let mut min_y = FT::INFINITY;
		for p in iter {
			max_x = max_x.max(p.x);
			min_x = min_x.min(p.x);
			max_y = max_y.max(p.y);
			min_y = min_y.min(p.y);
		}
		BoundingRect {
			min: Point2::new(min_x, min_y),
			max: Point2::new(max_x, max_y),
		}
	}
}

impl HasStart2 for Arc2 {
	fn start(&self) -> Point2 {
		self.start
	}
}

impl HasEnd2 for Arc2 {
	fn end(&self) -> Point2 {
		match self.arc_type {
			Arc2Type::Circle { .. } => self.start,
			Arc2Type::Partial { end, .. } => end,
		}
	}
}

impl HasMiddle2 for Arc2 {
	fn middle(&self) -> Point2 {
		self.point_at_angle(self.angle() * 0.5)
	}
}

impl HasLength for Arc2 {
	fn length(&self) -> FT {
		self.radius() * self.angle().abs()
	}
}

impl Reverse for Arc2 {
	fn reverse(&mut self) -> &mut Self {
		match &mut self.arc_type {
			Arc2Type::Circle { ccw, center: _ } => {
				*ccw = !*ccw;
			}
			Arc2Type::Partial { angle, end } => {
				*angle = -*angle;
				let new_start = *end;
				*end = self.start;
				self.start = new_start;
			}
		}
		self
	}
}

impl Flipable2 for Arc2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		self.start.flip(axis);
		self.arc_type.flip(axis);
		self
	}
}

impl GenerateOffset2 for Arc2 {
	fn generate_offset(&self, distance: FT) -> Option<Self> {
		let center = self.center();
		let start = self.start();
		let angle = self.angle();
		let radius_change = distance * self.angle().signum();
		let center_start_dir = UnitVector2::new_normalize(start - center);
		let new_start = Translation2::from(center_start_dir.into_inner() * radius_change) * start;
		Arc2::new(center, new_start, angle).map_or(None, |a| Some(a))
	}
}

impl SplitAt for Arc2 {
	fn split_elongate_at(self, distance: FT) -> (Option<Self>, Option<Self>) {
		let center = self.center();
		let start = self.start();
		let ccw = self.ccw();

		let split_point = self.point_at(distance);
		let first_arc = Arc2::new2(center, start, split_point, ccw).map_or(None, |a| Some(a));
		let second_arc = Arc2::new2(center, split_point, self.end(), ccw).map_or(None, |a| Some(a));

		if distance <= 0.0 {
			(None, second_arc)
		} else if distance >= self.length() {
			(first_arc, None)
		} else {
			(first_arc, second_arc)
		}
	}
}

impl DirectionAt for Arc2 {
	fn direction_at(&self, distance: FT) -> UnitVector2<FT> {
		let point0 = self.point_at(distance);
		let center_line = Line2::new(self.center(), point0).expect("Expected line to be valid");
		let point1 = center_line.move_point_rangle(point0, -1.0 * distance.signum() * self.angle().signum());

		UnitVector2::new_normalize(Vector2::from(point1.coords - point0.coords))
	}
}

impl RoundedEq for Arc2 {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool {
		self.start().rounded_eq(&other.start(), decimals) && self.center().rounded_eq(&other.center(), decimals) && self.angle().rounded_eq(&other.angle(), decimals)
	}
}

impl AbsDiffEq for Arc2 {
	type Epsilon = FT;

	fn default_epsilon() -> Self::Epsilon {
		FT::EPSILON
	}

	fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
		self.start().abs_diff_eq(&other.start, epsilon) && self.center().abs_diff_eq(&other.center(), epsilon) && self.angle().abs_diff_eq(&other.angle(), epsilon)
	}
}

impl IntersectsWith<Line2> for Arc2 {
	fn intersects_with(&self, other: &Line2) -> Vec<Intersection2> {
		// Arc center has to be (0,0) for the calculation
		let arc_transform = Translation2::from(self.center()) * Transform2::identity();
		let arc_transform_inv = arc_transform.try_inverse().expect("Expected inverse of translation to be successful");
		let mut line = other.clone();
		line.transform(&arc_transform_inv);
		let linear_equation = line.linear_equation();
		let mut arc = self.clone();
		arc.transform(&arc_transform_inv);

		let intersections = arc.intersection_linear_equation(linear_equation);

		intersections
			.iter()
			.map(|(p, distance_a, in_arc_bounds, crosses)| {
				let mut p = *p;
				p.transform(&arc_transform);
				Intersection2 {
					distance_a: *distance_a,
					distance_b: distance(&other.start(), &p) * calculate_distance_sign(other, p),
					in_bounds: point_in_line_bounds(&other, p) && *in_arc_bounds,
					crosses: *crosses,
					kind: Intersection2Kind::Point(PointIntersection2 {
						p,
						direction_a: self.direction_at(*distance_a),
						direction_b: other.direction(),
					}),
				}
			})
			.collect()
	}
}

impl IntersectsWith<Arc2> for Arc2 {
	fn intersects_with(&self, other: &Arc2) -> Vec<Intersection2> {
		// Arc center has to be (0,0) for the calculation
		let arc_transform = Translation2::from(self.center()) * Transform2::identity();
		let arc_transform_inv = arc_transform.try_inverse().expect("Expected inverse of translation to be successful");
		let mut arc0 = self.clone();
		arc0.transform(&arc_transform_inv);
		let mut arc1 = other.clone();
		arc1.transform(&arc_transform_inv);

		let center0 = arc0.center();
		let center1 = arc1.center();
		let r0 = self.radius();
		let r1 = other.radius();
		if abs_diff_eq!(center0, center1, epsilon = EPSILON) {
			return if abs_diff_eq!(r0, r1, epsilon = EPSILON) {
				/* let (dist_a_start, bounds_a_start) = arc0.arc_intersection_bounds_check(arc1.start());
				let (dist_a_end, bounds_a_end) = arc0.arc_intersection_bounds_check(arc1.end());
				let (dist_b_start, bounds_b_start) = arc1.arc_intersection_bounds_check(arc0.start());
				let (dist_b_end, bounds_b_end) = arc1.arc_intersection_bounds_check(arc0.end());
				let overlaps = bounds_a_start || bounds_a_end || bounds_b_start || bounds_b_end;

				let coincident = if overlaps {
					let self_angle = self.angle();
					let sa_sign = self_angle.signum();
					let other_angle = other.angle();
					let oa_sign = other_angle.signum();
					let ss_eq = abs_diff_eq!(self.start(), other.start(), epsilon = EPSILON);
					let se_eq = abs_diff_eq!(self.start(), other.end(), epsilon = EPSILON);
					let es_eq = abs_diff_eq!(self.end(), other.start(), epsilon = EPSILON);
					let ee_eq = abs_diff_eq!(self.end(), other.end(), epsilon = EPSILON);
					if ss_eq && sa_sign != oa_sign {
						if ee_eq {
							// Two points
							todo!()
						} else {
							if bounds_b_end {
								// One point and one overlap
								todo!()
							} else {
								// One point
								todo!()
							}
						}
					} else if bounds_a_start && bounds_a_end && bounds_b_start && bounds_b_end {
						// Two overlaps
						todo!()
					} else {
						todo!()
					}
				} else {
					CoincidentIntersection2::NoOverlap
				};

				vec![Intersection2 {
					distance_a: dist_a_start.min(dist_a_end),
					distance_b: dist_b_start.min(dist_b_end),
					in_bounds: overlaps,
					crosses: false,
					kind: Intersection2Kind::Coincident(coincident),
				}] */
				todo!()
			} else {
				Vec::new()
			};
		}

		//Based on https://en.wikipedia.org/wiki/Intersection_(geometry)#Two_circles
		let a = 2.0 * (center1.x - center0.x);
		let b = 2.0 * (center1.y - center0.y);
		let c = r0 * r0 - center0.x * center0.x - center0.y * center0.y - r1 * r1 + center1.x * center1.x + center1.y * center1.y;

		let intersections = arc0.intersection_linear_equation((a, b, c));

		intersections
			.iter()
			.map(|(p, distance_a, in_arc_a_bounds, crosses)| {
				let mut p = *p;
				p.transform(&arc_transform);
				let (distance_b, in_arc_b_bounds) = other.arc_intersection_bounds_check(p);
				Intersection2 {
					distance_a: *distance_a,
					distance_b: distance_b,
					in_bounds: *in_arc_a_bounds && in_arc_b_bounds,
					crosses: *crosses,
					kind: Intersection2Kind::Point(PointIntersection2 {
						p,
						direction_a: self.direction_at(*distance_a),
						direction_b: other.direction_at(distance_b),
					}),
				}
			})
			.collect()
	}
}

#[cfg(test)]
mod tests {
	use approx::assert_abs_diff_eq;
	use nalgebra::{UnitVector2, Vector2};

	use crate::{
		shapes2d::{arc2::Arc2Error, tests::assert_compare_intersections, Line2, Point2},
		traits::{DirectionAt, HasBounds2, HasStart2, Intersection2, Intersection2Kind, IntersectsWith, PointIntersection2, Reverse},
		EPSILON, PI,
	};

	use super::Arc2;

	#[test]
	fn err_for_zero_radius() {
		let result = Arc2::new(Point2::new(1.0, 1.0), Point2::new(1.0, 1.0), PI);
		assert!(matches!(result, Err(Arc2Error::ZeroRadius)));
	}

	#[test]
	fn err_for_zero_radius2() {
		let result = Arc2::new2(Point2::new(1.0, 1.0), Point2::new(1.0, 1.0), Point2::new(0.0, 1.0), true);
		assert!(matches!(result, Err(Arc2Error::ZeroRadius)));
	}

	#[test]
	fn err_for_zero_angle() {
		let result = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), 0.0);
		assert!(matches!(result, Err(Arc2Error::ZeroAngle)));
	}

	#[test]
	fn full_circle() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), 2.0 * PI).unwrap();
		assert_abs_diff_eq!(arc.angle(), 2.0 * PI, epsilon = EPSILON)
	}

	#[test]
	fn full_circle2() {
		let arc = Arc2::new2(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), Point2::new(2.0, 1.0), true).unwrap();
		assert_abs_diff_eq!(arc.angle(), 2.0 * PI, epsilon = EPSILON)
	}

	#[test]
	fn more_than_a_circle() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), 2.5 * PI).unwrap();
		assert_abs_diff_eq!(arc.angle(), 2.0 * PI, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_pi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_hpi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), 0.5 * PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_ohpi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), 1.5 * PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_m_pi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), -PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_m_hpi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), -0.5 * PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn center_calculation_m_ohpi() {
		let center = Point2::new(1.0, 1.0);
		let arc = Arc2::new(center, Point2::new(2.0, 2.0), -1.5 * PI).unwrap();
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON)
	}

	#[test]
	fn bounding_rect_small_ccw() {
		let p0 = Point2::new(1.0, 1.0);
		let p1 = Point2::new(2.0, 0.0);
		let arc = Arc2::new(p0, p1, 0.75 * PI).unwrap();
		let radius = arc.radius();
		let bounding_rect = arc.bounding_rect();
		assert_abs_diff_eq!(bounding_rect.min, Point2::new(1.0, 0.0), epsilon = EPSILON);
		assert_abs_diff_eq!(bounding_rect.max, Point2::new(1.0 + radius, 1.0 + radius), epsilon = EPSILON);
	}

	#[test]
	fn bounding_rect_small_cw() {
		let p0 = Point2::new(1.0, 1.0);
		let p1 = Point2::new(2.0, 0.0);
		let arc = Arc2::new(p0, p1, -0.75 * PI).unwrap();
		let radius = arc.radius();
		let bounding_rect = arc.bounding_rect();
		assert_abs_diff_eq!(bounding_rect.min, Point2::new(1.0 - radius, 1.0 - radius), epsilon = EPSILON);
		assert_abs_diff_eq!(bounding_rect.max, Point2::new(2.0, 1.0), epsilon = EPSILON);
	}

	#[test]
	fn bounding_rect_bigger_ccw() {
		let p0 = Point2::new(1.0, 1.0);
		let p1 = Point2::new(2.0, 0.0);
		let arc = Arc2::new(p0, p1, 1.0 * PI).unwrap();
		let radius = arc.radius();
		let bounding_rect = arc.bounding_rect();
		assert_abs_diff_eq!(bounding_rect.min, Point2::new(0.0, 0.0), epsilon = EPSILON);
		assert_abs_diff_eq!(bounding_rect.max, Point2::new(1.0 + radius, 1.0 + radius), epsilon = EPSILON);
	}

	#[test]
	fn point_at_angle_ccw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), PI).unwrap();
		let point = arc.point_at_angle(PI * 0.5);
		assert_abs_diff_eq!(point, Point2::new(1.0, 2.0), epsilon = EPSILON);
	}

	#[test]
	fn point_at_angle_cw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), -PI).unwrap();
		let point = arc.point_at_angle(-PI * 0.5);
		assert_abs_diff_eq!(point, Point2::new(1.0, 0.0), epsilon = EPSILON);
	}

	#[test]
	fn point_at_distance_ccw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), PI).unwrap();
		let point = arc.point_at(PI * 0.5);
		assert_abs_diff_eq!(point, Point2::new(1.0, 2.0), epsilon = EPSILON);
	}

	#[test]
	fn point_at_distance_cw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), -PI).unwrap();
		let point = arc.point_at(PI * 0.5);
		assert_abs_diff_eq!(point, Point2::new(1.0, 0.0), epsilon = EPSILON);
	}

	#[test]
	fn direction_at_start_ccw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 0.0), PI).unwrap();
		let direction = arc.direction_at(0.0);
		assert_abs_diff_eq!(direction, UnitVector2::new_normalize(Vector2::new(1.0, 1.0)), epsilon = EPSILON)
	}

	#[test]
	fn direction_at_start_cw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 0.0), -PI).unwrap();
		let direction = arc.direction_at(0.0);
		assert_abs_diff_eq!(direction, UnitVector2::new_normalize(Vector2::new(-1.0, -1.0)), epsilon = EPSILON)
	}

	#[test]
	fn direction_at_ccw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 0.0), PI).unwrap();
		let direction = arc.direction_at(0.25 * PI * arc.radius());
		assert_abs_diff_eq!(direction, UnitVector2::new_normalize(Vector2::new(0.0, 1.0)), epsilon = EPSILON)
	}

	#[test]
	fn direction_at_cw() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 0.0), PI).unwrap();
		let direction = arc.direction_at(-0.25 * PI * arc.radius());
		assert_abs_diff_eq!(direction, UnitVector2::new_normalize(Vector2::new(-1.0, 0.0)), epsilon = EPSILON)
	}

	#[test]
	fn reverse() {
		let center = Point2::new(1.0, 1.0);
		let mut arc = Arc2::new(center, Point2::new(2.0, 1.0), PI).unwrap();
		arc.reverse();
		assert_abs_diff_eq!(arc.angle(), -PI, epsilon = EPSILON);
		assert_abs_diff_eq!(arc.start(), Point2::new(0.0, 1.0), epsilon = EPSILON);
		assert_abs_diff_eq!(arc.center(), center, epsilon = EPSILON);
	}

	#[test]
	fn arc_intersection_bounds_check_in_bounds() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), PI).unwrap();
		let point = Point2::new(1.0, 2.0);
		let (distance, in_bounds) = arc.arc_intersection_bounds_check(point);
		assert_abs_diff_eq!(distance, PI * 0.5, epsilon = EPSILON);
		assert!(in_bounds);
	}

	#[test]
	fn arc_intersection_bounds_check_out_of_bounds() {
		let arc = Arc2::new(Point2::new(1.0, 1.0), Point2::new(2.0, 1.0), -PI).unwrap();
		let point = Point2::new(1.0, 2.0);
		let (distance, in_bounds) = arc.arc_intersection_bounds_check(point);
		assert_abs_diff_eq!(distance, PI * 1.5, epsilon = EPSILON);
		assert!(!in_bounds);
	}

	#[test]
	fn intersection_line_none() {
		let arc = Arc2::new(Point2::new(1.0, 2.0), Point2::new(2.0, 1.0), -PI).unwrap();
		let line = Line2::new(Point2::new(-5.0, -2.0), Point2::new(4.0, 1.0)).unwrap();

		let intersections = arc.intersects_with(&line);
		assert_eq!(intersections.len(), 0);
	}

	#[test]
	fn intersection_line_touch_in_bounds() {
		let arc = Arc2::new(Point2::new(1.0, 2.0), Point2::new(2.0, 2.0), -PI).unwrap();
		let line = Line2::new(Point2::new(-5.0, 1.0), Point2::new(1.0, 1.0)).unwrap();

		let intersections = arc.intersects_with(&line);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: PI * 0.5,
			distance_b: 6.0,
			in_bounds: true,
			crosses: false,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(1.0, 1.0),
				direction_a: UnitVector2::new_normalize(Vector2::new(-1.0, 0.0)),
				direction_b: UnitVector2::new_normalize(Vector2::new(1.0, 0.0)),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_touch_out_of_bounds() {
		let arc = Arc2::new(Point2::new(1.0, 2.0), Point2::new(2.0, 2.0), -PI * 0.25).unwrap();
		let line = Line2::new(Point2::new(-5.0, 1.0), Point2::new(1.0, 1.0)).unwrap();

		let intersections = arc.intersects_with(&line);
		assert_eq!(intersections.len(), 1);
		let intersection = &intersections[0];
		let c = Intersection2 {
			distance_a: PI * 0.5,
			distance_b: 6.0,
			in_bounds: false,
			crosses: false,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(1.0, 1.0),
				direction_a: UnitVector2::new_normalize(Vector2::new(-1.0, 0.0)),
				direction_b: UnitVector2::new_normalize(Vector2::new(1.0, 0.0)),
			}),
		};
		assert_compare_intersections(intersection, &c);
	}

	#[test]
	fn intersection_line_touch_dual_both_in_bounds() {
		/* let arc = Arc2::new(Point2::new(1.0, 2.0), Point2::new(2.0, 1.0), -PI * 0.25).unwrap();
		let line = Line2::new(Point2::new(-1.0, 0.0), Point2::new(5.0, 6.0)).unwrap();

		let intersections = arc.intersects_with(&line);
		assert_eq!(intersections.len(), 2);
		let intersection = &intersections[0];
		let c0 = Intersection2 {
			distance_a: PI * 0.5,
			distance_b: 6.0,
			in_bounds: false,
			crosses: false,
			kind: Intersection2Kind::Point(PointIntersection2 {
				p: Point2::new(1.0, 1.0),
				direction_a: UnitVector2::new_normalize(Vector2::new(-1.0, 0.0)),
				direction_b: UnitVector2::new_normalize(Vector2::new(1.0, 0.0)),
			}),
		};
		assert_compare_intersections(intersection, &c); */
		todo!()
	}

	#[test]
	fn intersection_line_touch_dual_one_in_bounds() {
		todo!()
	}

	#[test]
	fn intersection_line_touch_dual_out_of_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_none() {
		todo!()
	}

	#[test]
	fn intersection_arc_touch_in_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_touch_out_of_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_point_dual_both_in_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_point_dual_one_in_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_point_dual_out_of_bounds() {
		todo!()
	}

	#[test]
	fn intersection_arc_same_center_none() {
		todo!()
	}

	#[test]
	fn intersection_arc_same_center_coincident_in_bounds_stroke() {
		todo!()
	}

	#[test]
	fn intersection_arc_same_center_coincident_in_bounds_point() {
		todo!()
	}

	#[test]
	fn intersection_arc_same_center_coincident_out_of_bounds() {
		todo!()
	}
}
