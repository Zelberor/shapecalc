// Copyright 2024 Peter Viechter <zelberor@schnalzerbuam.de>.

mod arc2;
mod line2;
mod path2;

use approx::{abs_diff_eq, AbsDiffEq};
use nalgebra::{distance, Rotation2, Transform2, UnitVector2};
use std::cmp::Ordering;
use thiserror::Error;

use crate::{
	traits::{
		multiply_round_decimals, DirectionAt, Flipable2, GenerateOffset2, HasBounds2, HasEnd2, HasLength, HasMiddle2, HasStart2, Intersection2, IntersectsWith, RefDistanceOrd, Reverse, RoundedEq, RoundedOrd, SplitAt, TotalOrd,
		Transformable2,
	},
	EPSILON, FT,
};

pub use self::{
	arc2::Arc2,
	line2::Line2,
	path2::{Path2, Path2Order, Stroke2Info, Stroke2Iterator},
};
use self::{arc2::Arc2Error, line2::Line2Error};

/// Calculates the signed angle between two directions.
/// - Angle > 0: counter clockwise
/// - Angle < 0: clockwise
pub fn signed_angle_between(dir0: UnitVector2<FT>, dir1: UnitVector2<FT>) -> FT {
	(dir0.x * dir1.y - dir0.y * dir1.x).atan2(dir0.x * dir1.x + dir0.y * dir1.y)
}

pub type Point2 = nalgebra::Point2<FT>;

impl RefDistanceOrd for Point2 {
	fn ref_distance_cmp(&self, other: &Self, reference: &Self) -> Ordering {
		let dist1 = distance(self, reference);
		let dist2 = distance(other, reference);
		dist1.total_cmp(&dist2)
	}
}

impl RoundedEq for Point2 {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool {
		let self_x_rounded = multiply_round_decimals(self.x, decimals);
		let other_x_rounded = multiply_round_decimals(other.x, decimals);
		let self_y_rounded = multiply_round_decimals(self.y, decimals);
		let other_y_rounded = multiply_round_decimals(other.y, decimals);
		self_x_rounded == other_x_rounded && self_y_rounded == other_y_rounded
	}
}

impl RoundedOrd for Point2 {
	fn rounded_cmp(&self, other: &Self, decimals: u32) -> Ordering {
		// Todo: handle NaN and Inf cases

		let self_x_rounded = multiply_round_decimals(self.x, decimals);
		let other_x_rounded = multiply_round_decimals(other.x, decimals);

		match self_x_rounded.cmp(&other_x_rounded) {
			Ordering::Equal => {
				let self_y_rounded = multiply_round_decimals(self.y, decimals);
				let other_y_rounded = multiply_round_decimals(other.y, decimals);
				self_y_rounded.cmp(&other_y_rounded)
			}
			o => o,
		}
	}
}

impl TotalOrd for Point2 {
	fn total_cmp(&self, other: &Self) -> Ordering {
		match self.x.total_cmp(&other.x) {
			Ordering::Equal => self.y.total_cmp(&other.y),
			ord => ord,
		}
	}
}

impl Transformable2 for Point2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		let t_point = *transform * *self;
		self.x = t_point.x;
		self.y = t_point.y;
		self
	}
}

impl Flipable2 for Point2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		if abs_diff_eq!(*self, &Point2::new(0.0, 0.0), epsilon = EPSILON) {
			return self;
		}

		let self_dir = UnitVector2::new_normalize(self.coords);
		let angle = signed_angle_between(self_dir, axis);
		*self = Rotation2::new(2.0 * angle) * *self;

		self
	}
}

#[derive(Debug, Clone, Error)]
pub enum Stroke2Error {
	#[error("{0}")]
	Line2(#[from] Line2Error),
	#[error("{0}")]
	Arc2(#[from] Arc2Error),
	#[error("Unsupported stroke: {0}. Only Lines, Arcs and Circles are supported")]
	Unsupported(String),
}

/// A stroke. Guaranteed to have a non-zero length
#[derive(Clone, Debug, PartialEq)]
pub enum Stroke2 {
	Line2(Line2),
	Arc2(Arc2),
}

impl From<Line2> for Stroke2 {
	fn from(value: Line2) -> Self {
		Self::Line2(value)
	}
}

impl From<Arc2> for Stroke2 {
	fn from(value: Arc2) -> Self {
		Self::Arc2(value)
	}
}

impl HasBounds2 for Stroke2 {
	fn bounding_rect(&self) -> crate::traits::BoundingRect {
		match self {
			Stroke2::Line2(l) => l.bounding_rect(),
			Stroke2::Arc2(a) => a.bounding_rect(),
		}
	}
}

impl HasStart2 for Stroke2 {
	fn start(&self) -> Point2 {
		match self {
			Stroke2::Line2(l) => l.start(),
			Stroke2::Arc2(a) => a.start(),
		}
	}
}

impl HasEnd2 for Stroke2 {
	fn end(&self) -> Point2 {
		match self {
			Stroke2::Line2(l) => l.end(),
			Stroke2::Arc2(a) => a.end(),
		}
	}
}

impl HasMiddle2 for Stroke2 {
	fn middle(&self) -> Point2 {
		match self {
			Stroke2::Line2(l) => l.middle(),
			Stroke2::Arc2(a) => a.middle(),
		}
	}
}

impl HasLength for Stroke2 {
	fn length(&self) -> FT {
		match self {
			Stroke2::Line2(l) => l.length(),
			Stroke2::Arc2(a) => a.length(),
		}
	}
}

impl Reverse for Stroke2 {
	fn reverse(&mut self) -> &mut Self {
		match self {
			Stroke2::Line2(l) => {
				l.reverse();
			}
			Stroke2::Arc2(a) => {
				a.reverse();
			}
		}
		self
	}
}

impl Transformable2 for Stroke2 {
	fn transform(&mut self, transform: &Transform2<FT>) -> &mut Self {
		match self {
			Stroke2::Line2(l) => {
				l.transform(transform);
			}
			Stroke2::Arc2(a) => {
				a.transform(transform);
			}
		}
		self
	}
}

impl Flipable2 for Stroke2 {
	fn flip(&mut self, axis: UnitVector2<FT>) -> &mut Self {
		match self {
			Stroke2::Line2(l) => {
				l.flip(axis);
			}
			Stroke2::Arc2(a) => {
				a.flip(axis);
			}
		}
		self
	}
}

impl GenerateOffset2 for Stroke2 {
	fn generate_offset(&self, distance: FT) -> Option<Self> {
		match self {
			Stroke2::Line2(l) => l.generate_offset(distance).map(Into::into),
			Stroke2::Arc2(a) => a.generate_offset(distance).map(Into::into),
		}
	}
}

impl SplitAt for Stroke2 {
	fn split_elongate_at(self, distance: FT) -> (Option<Self>, Option<Self>) {
		match self {
			Stroke2::Line2(l) => {
				let (l0, l1) = l.split_elongate_at(distance);
				(l0.map(Into::into), l1.map(Into::into))
			}
			Stroke2::Arc2(a) => {
				let (a0, a1) = a.split_elongate_at(distance);
				(a0.map(Into::into), a1.map(Into::into))
			}
		}
	}
}

impl DirectionAt for Stroke2 {
	fn direction_at(&self, distance: FT) -> UnitVector2<FT> {
		match self {
			Stroke2::Line2(l) => l.direction_at(distance),
			Stroke2::Arc2(a) => a.direction_at(distance),
		}
	}
}

impl RoundedEq for Stroke2 {
	fn rounded_eq(&self, other: &Self, decimals: u32) -> bool {
		match self {
			Stroke2::Line2(l0) => match other {
				Stroke2::Line2(l1) => l0.rounded_eq(l1, decimals),
				_ => false,
			},
			Stroke2::Arc2(a0) => match other {
				Stroke2::Arc2(a1) => a0.rounded_eq(a1, decimals),
				_ => false,
			},
		}
	}
}

impl AbsDiffEq for Stroke2 {
	type Epsilon = FT;

	fn default_epsilon() -> Self::Epsilon {
		FT::EPSILON
	}

	fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
		match self {
			Stroke2::Line2(l0) => match other {
				Stroke2::Line2(l1) => l0.abs_diff_eq(l1, epsilon),
				_ => false,
			},
			Stroke2::Arc2(a0) => match other {
				Stroke2::Arc2(a1) => a0.abs_diff_eq(a1, epsilon),
				_ => false,
			},
		}
	}
}

#[cfg(feature = "dxf")]
impl TryFrom<&dxf::entities::EntityType> for Stroke2 {
	type Error = Stroke2Error;

	fn try_from(value: &dxf::entities::EntityType) -> Result<Self, Stroke2Error> {
		match value {
			dxf::entities::EntityType::Line(ref line) => match Line2::try_from(line) {
				Ok(l) => Ok(l.into()),
				Err(e) => Err(e.into()),
			},
			dxf::entities::EntityType::Arc(ref arc) => match Arc2::try_from(arc) {
				Ok(a) => Ok(a.into()),
				Err(e) => Err(e.into()),
			},
			dxf::entities::EntityType::Circle(ref circle) => match Arc2::try_from(circle) {
				Ok(c) => Ok(c.into()),
				Err(e) => Err(e.into()),
			},
			e => Err(Stroke2Error::Unsupported(format!("{:?}", e))),
		}
	}
}

impl IntersectsWith<Stroke2> for Stroke2 {
	fn intersects_with(&self, other: &Stroke2) -> Vec<Intersection2> {
		match self {
			Stroke2::Line2(l0) => match other {
				Stroke2::Line2(l1) => l0.intersects_with(l1),
				Stroke2::Arc2(a1) => l0.intersects_with(a1),
			},
			Stroke2::Arc2(a0) => match other {
				Stroke2::Line2(l1) => a0.intersects_with(l1),
				Stroke2::Arc2(a1) => a0.intersects_with(a1),
			},
		}
	}
}

impl IntersectsWith<Path2> for Stroke2 {
	fn intersects_with(&self, other: &Path2) -> Vec<Intersection2> {
		other
			.intersects_with(self)
			.into_iter()
			.map(|mut i| {
				i.flip_a_b();
				i
			})
			.collect()
	}
}

#[cfg(test)]
mod tests {
	use approx::assert_abs_diff_eq;

	use crate::{
		traits::{CoincidentIntersection2, HasEnd2, HasStart2, Intersection2, Intersection2Kind},
		EPSILON,
	};

	use super::Stroke2;

	pub fn assert_compare_strokes(s0: &Stroke2, s1: &Stroke2) {
		let result = match s0 {
			Stroke2::Line2(l0) => match s1 {
				Stroke2::Line2(l1) => {
					assert_abs_diff_eq!(l0.start(), l1.start(), epsilon = EPSILON);
					assert_abs_diff_eq!(l0.end(), l1.end(), epsilon = EPSILON);
					Ok(())
				}
				Stroke2::Arc2(_) => Err(()),
			},
			Stroke2::Arc2(a0) => match s1 {
				Stroke2::Line2(_) => Err(()),
				Stroke2::Arc2(a1) => {
					assert_abs_diff_eq!(a0.center(), a1.center(), epsilon = EPSILON);
					assert_abs_diff_eq!(a0.start(), a1.start(), epsilon = EPSILON);
					assert_abs_diff_eq!(a0.angle(), a1.angle(), epsilon = EPSILON);
					Ok(())
				}
			},
		};
		assert!(result.is_ok(), "Stroke types must match, but are: s0: {:?}, s1: {:?}", s0, s1);
	}

	pub fn assert_compare_intersections(i0: &Intersection2, i1: &Intersection2) {
		assert_abs_diff_eq!(i0.distance_a, i1.distance_a, epsilon = EPSILON);
		assert_abs_diff_eq!(i0.distance_b, i1.distance_b, epsilon = EPSILON);
		assert!(i0.in_bounds == i1.in_bounds);
		assert!(i0.crosses == i1.crosses);
		let result = match &i0.kind {
			Intersection2Kind::Point(p0) => match &i1.kind {
				Intersection2Kind::Point(p1) => {
					assert_abs_diff_eq!(p0.direction_a, p1.direction_a, epsilon = EPSILON);
					assert_abs_diff_eq!(p1.direction_b, p1.direction_b, epsilon = EPSILON);
					assert_abs_diff_eq!(p0.p, p1.p, epsilon = EPSILON);
					Ok(())
				}
				Intersection2Kind::Coincident(_) => Err(()),
			},
			Intersection2Kind::Coincident(c0) => match &i1.kind {
				Intersection2Kind::Point(_) => Err(()),
				Intersection2Kind::Coincident(c1) => match c0 {
					CoincidentIntersection2::Overlap { o_a: oa0, o_b: ob0 } => {
						if let CoincidentIntersection2::Overlap { o_a: oa1, o_b: ob1 } = c1 {
							assert_compare_strokes(oa0, oa1);
							assert_compare_strokes(ob0, ob1);
							Ok(())
						} else {
							Err(())
						}
					}
					CoincidentIntersection2::NoOverlap => {
						if matches!(c1, CoincidentIntersection2::NoOverlap) {
							Ok(())
						} else {
							Err(())
						}
					}
					CoincidentIntersection2::Point(p0) => {
						if let CoincidentIntersection2::Point(p1) = c1 {
							assert_abs_diff_eq!(p0, p1, epsilon = EPSILON);
							Ok(())
						} else {
							Err(())
						}
					}
				},
			},
		};
		assert!(result.is_ok(), "Intersection kinds must match, but are: i0: {:?}, i1: {:?}", i0, i1);
	}
}
